<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'type',
        'user_id',
        'author_id',
        'recipient_id',
        'post_uuid',
        'seen'
    ];

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'author_id');
    }
    public function recipient()
    {
        return $this->hasOne('App\User', 'id', 'recipient_id');
    }
    public function post()
    {
        return $this->hasOne('App\Post', 'uuid', 'post_uuid');
    }
}
