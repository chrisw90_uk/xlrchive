<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    protected $appends = ['post_preview'];

    protected $fillable = [
        'uuid',
        'author_id',
        'recipient_id',
        'content',
    ];

    public function comments()
    {
        return $this->hasMany('App\Comment', 'post_uuid', 'uuid' );
    }
    public function votes()
    {
        return $this->hasMany('App\Vote', 'post_uuid', 'uuid' );
    }
    public function has_voted()
    {
        $user = Auth::user();
        return $this
            ->hasOne('App\Vote', 'post_uuid', 'uuid' )
            ->where('user_id','=', $user['id']);
    }

    public function getPostPreviewAttribute()
    {
        $content = $this->content;
        return $this->attributes['post_preview'] = strlen($content) > 40 ? substr($content, 0, 40).'...' : $content;
    }
}
