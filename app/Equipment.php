<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    protected $fillable = [
        'uuid',
        'equipment_category_uuid',
        'user_id',
        'name',
        'image_url',
        'primary_category',
        'secondary_category',
        'position'
    ];

    public function reactions()
    {
        return $this->hasMany('App\Reaction','equipment_uuid', 'uuid');
    }
}
