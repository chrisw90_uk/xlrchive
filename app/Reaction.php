<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reaction extends Model
{
    protected $fillable = [
        'uuid',
        'user_id',
        'equipment_uuid',
        'reaction_type',
    ];
}
