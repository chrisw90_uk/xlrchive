<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = [
        'uuid',
        'user_id',
        'post_uuid',
        'comment_uuid',
    ];
}
