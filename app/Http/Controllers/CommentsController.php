<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Mail\NewCommentOnForeignPost;
use App\Mail\NewCommentOnProfile;
use App\User;
use App\Vote;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{
    public function submitComment(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'authorId' => ['required', 'string', 'max:11'],
                'recipientId' => ['required', 'string', 'max:11'],
                'postAuthorId' => ['required', 'integer', 'max:11'],
                'postId' => ['required', 'string', 'max:255'],
                'value' => ['required', 'string'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $next_id = Comment::get()->count() + 1;
            $authorId = $request->get('authorId');
            $recipientId = $request->get('recipientId');
            $postAuthorId = $request->get('postAuthorId');
            $postId = $request->get('postId');
            $comment = Comment::create([
                'uuid' => Hash::make($next_id),
                'post_uuid' => $postId,
                'author_id' => $authorId,
                'recipient_id' => $request->get('recipientId'),
                'post_author_id' => $postAuthorId,
                'content' => $request->get('value'),
            ]);
            if ($recipientId !== $authorId) {
                $this->createNotification(config('notifications.comment'), $recipientId, $authorId, $recipientId, $postId);
                $recipient = User::where('id', $recipientId)->firstOrFail();
                if($recipient['email_notifications']) {
                    Mail::to($recipient['email'])->send(new NewCommentOnProfile(
                        $recipient['username'],
                        Auth::user()['username']
                    ));
                }
            }
            if ($recipientId !== $postAuthorId && $authorId !== $postAuthorId) {
                $this->createNotification(config('notifications.comment_foreign'), $postAuthorId, $authorId, $recipientId, $postId);
                $post_author = User::where('id', $postAuthorId)->firstOrFail();
                if($post_author['email_notifications']) {
                    $recipient = User::where('id', $recipientId)->firstOrFail();
                    Mail::to($post_author['email'])->send(new NewCommentOnForeignPost(
                        $post_author['username'],
                        Auth::user()['username'],
                        $recipient['username']
                    ));
                }
            }
            return Comment::join('users', 'comments.author_id', '=', 'users.id')
                ->where('comments.uuid', $comment['uuid'])
                ->select(
                    'comments.uuid',
                    'users.username as author',
                    'users.profile_image_url as author_image_url',
                    'comments.author_id',
                    'comments.content',
                    'comments.created_at'
                )
                ->withCount('votes')
                ->withCount('has_voted')
                ->firstOrFail();
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function deleteComment(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'uuid' => ['required', 'string', 'max:255']
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            Vote::where('comment_uuid', $request->get('uuid'))->delete();
            Comment::where('uuid', $request->get('uuid'))->delete();
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
