<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Notification;
use App\Vote;
use App\User;
use App\Post;
use App\Mail\NewPostOnProfile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{
    public function getPosts ($recipientId) {
        try {
            $posts = Post::join('users', 'posts.author_id', '=', 'users.id')
                ->where('posts.recipient_id', $recipientId)
                ->select(
                    'posts.uuid',
                    'posts.author_id as author_id',
                    'users.username as author',
                    'users.profile_image_url as author_image_url',
                    'posts.author_id',
                    'posts.content',
                    'posts.created_at'
                )
                ->withCount('votes')
                ->withCount('has_voted')
                ->orderBy('posts.created_at', 'desc')
                ->get();
            foreach($posts as $post) {
                $post['comments'] = Comment::join('users', 'comments.author_id', '=', 'users.id')
                    ->where('post_uuid', $post['uuid'])
                    ->select(
                        'comments.uuid',
                        'users.username as author',
                        'users.profile_image_url as author_image_url',
                        'comments.author_id',
                        'comments.content',
                        'comments.created_at'
                    )
                    ->withCount('votes')
                    ->withCount('has_voted')
                    ->orderBy('comments.created_at', 'asc')
                    ->get();
            }
            return $posts;
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }
    public function submitPost(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'authorId' => ['required', 'string', 'max:255'],
                'recipientId' => ['required', 'string', 'max:255'],
                'value' => ['required', 'string'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $authorId = $request->get('authorId');
            $recipientId = $request->get('recipientId');
            $next_id = Post::get()->count() + 1;
            $post_uuid = Hash::make($next_id);

            $post = Post::create([
                'uuid' => $post_uuid,
                'author_id' => $authorId,
                'recipient_id' => $recipientId,
                'content' => $request->get('value')
            ]);

            if ($authorId !== $recipientId) {
                $this->createNotification(config('notifications.post'), $recipientId, $authorId, $recipientId, $post_uuid);
                $recipient = User::where('id', $recipientId)->firstOrFail();
                if($recipient['email_notifications']) {
                    Mail::to($recipient['email'])->send(new NewPostOnProfile(
                        $recipient['username'],
                        Auth::user()['username']
                    ));
                }
            }

            $post = Post::join('users', 'posts.author_id', '=', 'users.id')
                ->where('posts.uuid', $post['uuid'])
                ->select(
                    'posts.uuid',
                    'users.username as author',
                    'users.profile_image_url as author_image_url',
                    'posts.author_id',
                    'posts.content',
                    'posts.created_at'
                )
                ->firstOrFail();
            $post['comments'] = [];
            $post['votes_count'] = 0;
            $post['has_voted_count'] = 0;
            return $post;
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function deletePost(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'uuid' => ['required', 'string', 'max:255']
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            Post::where('uuid', $request->get('uuid'))->delete();
            Comment::where('post_uuid', $request->get('uuid'))->delete();
            Vote::where('post_uuid', $request->get('uuid'))->delete();
            Notification::where('post_uuid', $request->get('uuid'))->delete();
            return response()->json([
                'messages' => ['Post deleted successfully'],
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
