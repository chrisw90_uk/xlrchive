<?php

namespace App\Http\Controllers;

use App\Reaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ReactionsController extends Controller
{
    public function toggleReactions(Request $request) {
        try {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'enableReactions' => ['required', 'boolean'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            return User::where('id', $user['id'])->update([
                'enable_reactions' => $request->get('enableReactions')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function addOrUpdateReaction(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'uuid' => ['nullable', 'string', 'max:255'],
                'equipmentId' => ['required', 'string', 'max:255'],
                'reactingUserId' => ['required','integer'],
                'reactionType' => ['required', 'string', 'max:10'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $uuid = $request->get('uuid');
            if(is_null($uuid)) {
                $uuid = Hash::make(Reaction::get()->count() + 1);
            }
            return Reaction::updateOrCreate(
                ['uuid' => $uuid],
                [
                    'uuid' => $uuid,
                    'equipment_uuid' => $request->get('equipmentId'),
                    'user_id' => $request->get('reactingUserId'),
                    'reaction_type' => $request->get('reactionType'),
                ]
            );
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function removeReaction(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'id' => ['required', 'string', 'max:255'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            return Reaction::where('uuid', $request->get('id'))->delete();
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
