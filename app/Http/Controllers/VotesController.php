<?php

namespace App\Http\Controllers;

use App\Vote;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class VotesController extends Controller
{
    //
    public function submitVote(Request $request) {
        try {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'postId' => ['string', 'max:255', 'nullable'],
                'commentId' => ['string', 'max:255', 'nullable'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $next_id = Vote::get()->count() + 1;

            return Vote::create([
                'uuid' => Hash::make($next_id),
                'user_id' => $user['id'],
                'post_uuid' => $request->get('postId'),
                'comment_uuid' => $request->get('commentId'),
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function deleteVote(Request $request) {
        try {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'postId' => ['string', 'max:255', 'nullable'],
                'commentId' => ['string', 'max:255', 'nullable']
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            Vote::where([
                'post_uuid' => $request->get('postId'),
                'comment_uuid' => $request->get('commentId'),
                'user_id' => $user['id']
            ])->delete();
            return response()->json([
                'messages' => ['Vote removed successfully'],
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
