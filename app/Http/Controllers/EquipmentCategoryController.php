<?php

namespace App\Http\Controllers;

use App\Equipment;
use Illuminate\Http\Request;
use App\EquipmentCategory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class EquipmentCategoryController extends Controller
{
    /**
     * Get equipment categories
     */
    public function getEquipmentCategories() {
        $user = Auth::user();
        $equipment = EquipmentCategory::where('user_id', $user['id'])
            ->with('equipment')
            ->select('uuid', 'name', 'position')
            ->orderBy('created_at', 'asc')
            ->get();

        return response()->json([
            'enable_reactions' => $user['enable_reactions'],
            'equipment' => $equipment,
        ], Response::HTTP_OK);
    }

    /**
     * Save equipment categories
     */
    public function saveEquipmentCategory(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'position' => ['required', 'integer']
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $user = Auth::user();
            $next_id = EquipmentCategory::get()->count() + 1;
            $cat = EquipmentCategory::create([
                'uuid' => Hash::make($next_id),
                'user_id' => $user['id'],
                'name' => $request->get('name'),
                'position' => $request->get('position'),
            ]);
            $cat['equipment'] = array();
            return $cat;
        } catch (\Exception $e) {
            return response()->json([
                'errors' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update equipment category
     */
    public function updateEquipmentCategory(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'id' => ['required', 'string', 'max:255'],
                'name' => ['required', 'string', 'max:255'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            return EquipmentCategory::where('uuid', $request->get('id'))->update([
                'name' => $request->get('name')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
    /**
     * Delete equipment category
     */
    public function deleteEquipmentCategory(Request $request) {
        try {
            $equipmentCategoryId = $request->get('id');
            EquipmentCategory::where('uuid', $equipmentCategoryId)->delete();
            Equipment::where('equipment_category_uuid', $equipmentCategoryId)->delete();
            return response()->json([
                'messages' => 'Category was deleted successfully',
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
