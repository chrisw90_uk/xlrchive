<?php

namespace App\Http\Controllers;

use App\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class NotificationsController extends Controller
{
    public function getNotifications() {
        try {
            $user = Auth::user();
            $notifications = Notification
                ::with('author:id,username,profile_image_url')
                ->with('recipient:id,username')
                ->with('post:uuid,content')
                ->where([
                    'user_id' => $user['id'],
                    'seen' => false,
                ])
                ->whereDate('created_at', Carbon::today())
                ->select(
                    'id',
                    'type',
                    'user_id',
                    'author_id',
                    'recipient_id',
                    'post_uuid',
                    'seen',
                    'updated_at as date'
                )
                ->orderByDesc('updated_at')
                ->get();
            return [
                'notifications' => $notifications,
                'unreadNotificationCount' => $notifications->count()
            ];
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function getNotificationCount() {
        try {
            $user = Auth::user();
            return Notification
                ::where([
                    'user_id' => $user['id'],
                    'seen' => false,
                ])
                ->whereDate('created_at', Carbon::today())
                ->count();
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function deleteNotification(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'id' => ['required', 'integer', 'max:255']
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            Notification::where('id', $request->get('id'))->delete();
            return response()->json([
                'messages' => ['Notification deleted successfully'],
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }
    public function markNotificationsAsRead(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'ids' => 'required:array',
                'ids.*'  => 'required|integer|distinct'
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            Notification::whereIn('id', $request->get('ids'))->update(['seen' => true]);
            return response()->json([
                'messages' => ['Notifications marked as read'],
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
