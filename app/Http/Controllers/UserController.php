<?php

namespace App\Http\Controllers;

use App\Equipment;
use App\EquipmentCategory;
use App\Notification;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    /**
     * Show the user profile
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showUserProfile($username)
    {
        $authUser = Auth::user();
        $user = User::where('username', $username)
            ->with([
                'equipment_categories',
                'equipment_categories.equipment',
                'equipment_categories.equipment.reactions'
            ])
            ->whereNull('deactivated')->first();
        if (is_null($user)) {
            return view('user.not-found');
        }
        $reactions = [];
        $reactionsFromAuthedUser = [];

        foreach ($user['equipment_categories'] as $cat) {
            foreach ($cat['equipment'] as $e) {
                array_push($reactions, [
                    'equipment_uuid' => $e['uuid'],
                    'reactions' => [
                        'like' => count($e['reactions']->filter(function($r){ return $r['reaction_type'] === 'like'; })),
                        'love' => count($e['reactions']->filter(function($r){ return $r['reaction_type'] === 'love'; })),
                        'awe' => count($e['reactions']->filter(function($r){ return $r['reaction_type'] === 'awe'; })),
                        'metal' => count($e['reactions']->filter(function($r){ return $r['reaction_type'] === 'metal'; }))
                    ]
                ]);
                if ($authUser) {
                    foreach ($e['reactions'] as $r) {
                        if ($r['user_id'] === $authUser['id']) {
                            array_push($reactionsFromAuthedUser, [
                                'equipment_uuid' => $e['uuid'],
                                'uuid' => $r['uuid'],
                                'reaction_type' => $r['reaction_type']
                            ]);
                        }
                    }
                }
            }
        }

        return view('user.profile', [
            'user' => $user,
            'auth_user_id' => $authUser['id'],
            'reactionCounts' => $reactions,
            'reactions' => $reactionsFromAuthedUser
        ]);
    }

    /**
     * Get user details
     */
    public function getUserDetails() {
        $user = Auth::user();
        return [
            'name' => $user['name'],
            'username' => $user['username'],
            'display_name' => $user['display_name'],
            'use_as_display_name' => $user['use_as_display_name'],
            'profile_image_url' => $user['profile_image_url'],
            'display_image_url' => $user['display_image_url'],
            'headline' => $user['headline'],
            'category' => $user['category'],
            'bio' => $user['bio'],
            'facebook' => $user['facebook'],
            'twitter' => $user['twitter'],
            'instagram' => $user['instagram'],
            'youtube' => $user['youtube']
        ];
    }

    /**
     * Save user details
     */
    public function saveUserDetails(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'display_name' => ['nullable', 'string', 'max:255'],
                'headline' => ['nullable', 'string', 'max:255'],
                'category' => ['nullable', 'string', 'max:255'],
                'bio' => ['nullable', 'string'],
                'facebook' => ['nullable', 'string', 'max:255'],
                'twitter' => ['nullable', 'string', 'max:255'],
                'instagram' => ['nullable', 'string', 'max:255'],
                'youtube' => ['nullable', 'string', 'max:255'],
                'profileImage' => 'array',
                'profileImage.imageToUpload' => ['nullable', 'string'],
                'profileImage.previousImage' => ['nullable', 'string'],
                'profileImage.isImageRemoved' => ['boolean'],
                'displayImage' => 'array',
                'displayImage.imageToUpload' => ['nullable', 'string'],
                'displayImage.previousImage' => ['nullable', 'string'],
                'displayImage.isImageRemoved' => ['boolean'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $user = Auth::user();

            // Profile image upload
            $profileImage = $request->get('profileImage');
            $originalImage = $profileImage['currentImageUrl'];
            $upload = $profileImage['imageToUpload'];
            $profileImageToSave = $originalImage;
            if($profileImage['isImageRemoved'] && !is_null($originalImage)) {
                $existingImage = parse_url($originalImage)['path'];
                $existingImage = ltrim($existingImage, $existingImage[0]);
                Storage::disk('s3')->delete($existingImage);
                $profileImageToSave = null;
            }

            if(!is_null($upload)){
                $file_name = $user['username'].'_profile-image_'.time().'.jpg';
                @list($type, $upload) = explode(';', $upload);
                @list(, $upload) = explode(',', $upload);
                $path = 'images/'.$file_name;
                if(Storage::disk('s3')->put('images/'.$file_name, base64_decode($upload), 'public')){
                    $profileImageToSave = Storage::disk('s3')->url($path);
                };
            }

            // Display image upload

            $displayImage = $request->get('displayImage');
            $originalImage = $displayImage['currentImageUrl'];
            $upload = $displayImage['imageToUpload'];
            $displayImageToSave = $originalImage;
            if($displayImage['isImageRemoved'] && !is_null($originalImage)) {
                $existingImage = parse_url($originalImage)['path'];
                $existingImage = ltrim($existingImage, $existingImage[0]);
                Storage::disk('s3')->delete($existingImage);
                $displayImageToSave = null;
            }

            if(!is_null($upload)){
                $file_name = $user['username'].'_header-image_'.time().'.jpg';
                @list($type, $upload) = explode(';', $upload);
                @list(, $upload) = explode(',', $upload);
                $path = 'images/'.$file_name;
                if(Storage::disk('s3')->put('images/'.$file_name, base64_decode($upload), 'public')){
                    $displayImageToSave = Storage::disk('s3')->url($path);
                };
            }

            return User::where('id', $user['id'])->update([
                'display_name' => $request->get('display_name'),
                'profile_image_url' => $profileImageToSave,
                'display_image_url' => $displayImageToSave,
                'headline' => $request->get('headline'),
                'category' => $request->get('category'),
                'bio' => $request->get('bio'),
                'use_as_display_name' => $request->get('use_as_display_name'),
                'facebook' => $request->get('facebook'),
                'twitter' => $request->get('twitter'),
                'instagram' => $request->get('instagram'),
                'youtube' => $request->get('youtube'),
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'errors' => array($e->getMessage()),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get account details
     */
    public function getAccountDetails() {
        $user = Auth::user();
        $notifications = Notification
            ::with('author:id,username,profile_image_url')
            ->with('recipient:id,username')
            ->with('post:uuid,content')
            ->where([
                'user_id' => $user['id'],
            ])
            ->select(
                'id',
                'type',
                'user_id',
                'author_id',
                'recipient_id',
                'post_uuid',
                'seen',
                'updated_at as date'
            )
            ->orderByDesc('updated_at')
            ->get();
        $notificationCount = count(array_filter($notifications->all(), function ($n) {
            return $n['seen'] === 0;
        }));
        return [
            'name' => $user['name'],
            'username' => $user['username'],
            'email' => $user['email'],
            'emailNotifications' => boolval($user['email_notifications']),
            'excludeFromSearch' => boolval($user['exclude_from_search']),
            'notifications' => $notifications,
            'unreadNotificationCount' => $notificationCount
        ];
    }

    /**
     * Save account changes
     */
    public function saveAccountDetails(Request $request) {
        try {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string', 'alpha_dash', 'max:255', 'unique:users,username,'.$user['id']],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$user['id']],
                'emailNotifications' => ['boolean'],
                'excludeFromSearch' => ['boolean'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            return User::where('id', $user['id'])->update([
                'name' => $request->get('name'),
                'username' => $request->get('username'),
                'email' => $request->get('email'),
                'email_notifications' => $request->get('emailNotifications'),
                'exclude_from_search' => $request->get('excludeFromSearch')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => array($e->getMessage()),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Save new password
     */
    public function saveNewPassword(Request $request) {
        try {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'currentPassword' => ['required', 'string'],
                'password' => ['required', 'string', 'min:8', 'regex:/[A-Z0-9]/', 'confirmed'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $pass_check = Auth::validate(
                ['email' => $user['email'], 'password' => $request->get('currentPassword')]
            );
            if(!$pass_check){
                return response()->json([
                    'errors' => ['Your current password is incorrect'],
                ], Response::HTTP_FORBIDDEN);
            }
            User::where('id', $user['id'])->update([
                'password' => Hash::make($request->get('password')),
            ]);
            if (Auth::attempt(['email' => $user->email, 'password' => $request->get('password')])) {
                return Response::HTTP_OK;
            } else {
                return response()->json([
                    'errors' => 'Unauthenticated',
                ], Response::HTTP_UNAUTHORIZED);
            }
        } catch (\Exception $e) {
            return response()->json([
                'errors' => array($e->getMessage()),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
    /**
     * Deactivate account
     */
    public function deactivateAccount(Request $request) {
        try {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'password' => ['required', 'string', 'confirmed']
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $pass_check = Auth::validate(
                ['email' => $user['email'], 'password' => $request->get('password')]
            );
            if(!$pass_check){
                return response()->json([
                    'errors' => ['Your current password is incorrect'],
                ], Response::HTTP_FORBIDDEN);
            }
            User::where('id', $user['id'])->update([
                'deactivated' => true
            ]);
            auth()->logout();
            return response()->json([
                'errors' => array('Account deactivated successfully'),
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => array($e->getMessage()),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Deactivate account
     */
    public function deleteAccount(Request $request) {
        try {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'password' => ['required', 'string', 'confirmed']
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $pass_check = Auth::validate(
                ['email' => $user['email'], 'password' => $request->get('password')]
            );
            if(!$pass_check){
                return response()->json([
                    'errors' => ['Your current password is incorrect'],
                ], Response::HTTP_FORBIDDEN);
            }
            $usersEquipmentCategories = EquipmentCategory::where('user_id', $user['id'])->get();
            forEach ($usersEquipmentCategories as $cat) {
                //Delete equipment images
                $equipment = Equipment::where('equipment_category_uuid', $cat['uuid'])->get();
                foreach ($equipment as $e) {
                    if (!is_null($e['image_url'])) {
                        $imagePath = parse_url($e['image_url'])['path'];
                        $imagePath = ltrim($imagePath, $imagePath[0]);
                        Storage::disk('s3')->delete($imagePath);
                    }
                }
                //Delete equipment
                Equipment::where('equipment_category_uuid', $cat['uuid'])->delete();
            }

            //Delete equipment categories
            EquipmentCategory::where('user_id', $user['id'])->delete();

            //Delete profile image
            if (!is_null($user['profile_image_url'])) {
                $imagePath = parse_url($user['profile_image_url'])['path'];
                $imagePath = ltrim($imagePath, $imagePath[0]);
                Storage::disk('s3')->delete($imagePath);
            }

            //Delete header image
            if (!is_null($user['display_image_url'])) {
                $imagePath = parse_url($user['display_image_url'])['path'];
                $imagePath = ltrim($imagePath, $imagePath[0]);
                Storage::disk('s3')->delete($imagePath);
            }
            //Delete user
            User::where('id', $user['id'])->delete();
            auth()->logout();
            return response()->json([
                'errors' => array('Account deleted successfully'),
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => array($e->getMessage()),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
