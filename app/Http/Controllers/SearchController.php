<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function showUsers(Request $request)
    {
        $displayName = $request->get('displayName');
        $category = $request->get('category');
        $search = [
            ['display_name', 'LIKE', '%'.$displayName.'%'],
            ['exclude_from_search', '=', 0]
        ];
        if ($category) {
            array_push($search, ['category', '=', $category]);
        };
        $users = User::where(function($query) use ($search)
            {
                $query->where($search);
            })
            ->simplePaginate(12);
        return view('user.search', ['users' => $users])->with([
            ['displayName', $displayName],
            ['category', $category]
        ]);
    }
}
