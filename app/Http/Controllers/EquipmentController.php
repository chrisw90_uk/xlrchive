<?php

namespace App\Http\Controllers;

use App\Equipment;
use App\Reaction;
use Illuminate\Http\Request;
use App\EquipmentCategory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EquipmentController extends Controller
{

    /**
     * Save equipment
     */
    public function saveEquipment(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'equipmentCategoryId' => ['required', 'string', 'max:255'],
                'name' => ['required', 'string', 'max:255'],
                'imageToUpload' => ['string', 'nullable'],
                'primaryCategory' => ['required', 'string', 'max:255'],
                'secondaryCategory' => ['required', 'string', 'max:255'],
                'position' => ['required', 'integer']
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $user = Auth::user();
            $next_id = Equipment::get()->count() + 1;

            //Image upload

            $upload = $request->get('imageToUpload');
            $uploadedImage = null;

            if(!is_null($upload)){
                $file_name = $user['username'].'_image_'.time().'.jpg';
                @list($type, $upload) = explode(';', $upload);
                @list(, $upload) = explode(',', $upload);
                $path = 'images/'.$file_name;
                if(Storage::disk('s3')->put('images/'.$file_name, base64_decode($upload), 'public')){
                    $uploadedImage = Storage::disk('s3')->url($path);
                };
            }

            return Equipment::create([
                'uuid' => Hash::make($next_id),
                'equipment_category_uuid' => $request->get('equipmentCategoryId'),
                'user_id' => $user['id'],
                'name' => $request->get('name'),
                'image_url' => $uploadedImage,
                'primary_category' => $request->get('primaryCategory'),
                'secondary_category' => $request->get('secondaryCategory'),
                'position' => $request->get('position'),
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Save equipment
     */
    public function updateEquipment(Request $request) {
        try {
            $user = Auth::user();
            $validator = Validator::make($request->all(), [
                'id' => ['required', 'string', 'max:255'],
                'name' => ['required', 'string', 'max:255'],
                'primaryCategory' => ['required', 'string', 'max:255'],
                'imageUrl' => ['string', 'nullable'],
                'imageToUpload' => ['string', 'nullable'],
                'isImageRemoved' => ['required', 'boolean'],
                'secondaryCategory' => ['required', 'string', 'max:255'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            //Image delete and upload

            $originalImage = $request->get('imageUrl');
            $upload = $request->get('imageToUpload');
            $imageToSave = $originalImage;
            if($request->get('isImageRemoved') && !is_null($originalImage)) {
                $existingImage = parse_url($originalImage)['path'];
                $existingImage = ltrim($existingImage, $existingImage[0]);
                Storage::disk('s3')->delete($existingImage);
                $imageToSave = null;
            }

            if(!is_null($upload)){
                $file_name = $user['username'].'_image_'.time().'.jpg';
                @list($type, $upload) = explode(';', $upload);
                @list(, $upload) = explode(',', $upload);
                $path = 'images/'.$file_name;
                if(Storage::disk('s3')->put('images/'.$file_name, base64_decode($upload), 'public')){
                    $imageToSave = Storage::disk('s3')->url($path);
                };
            }

            Equipment::where('uuid', $request->get('id'))->update([
                'name' => $request->get('name'),
                'image_url' => $imageToSave,
                'primary_category' => $request->get('primaryCategory'),
                'secondary_category' => $request->get('secondaryCategory'),
            ]);

            return [
                'updated' => true,
                'imageUrl' => $imageToSave,
            ];
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [$e->getMessage()],
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete equipment
     */
    public function deleteEquipment(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'id' => ['required', 'string', 'max:255'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()->all(),
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $id = $request->get('id');
            $e = Equipment::where('uuid', $id)->firstOrFail();
            if (!is_null($e['image_url'])) {
                $imagePath = parse_url($e['image_url'])['path'];
                $imagePath = ltrim($imagePath, $imagePath[0]);
                Storage::disk('s3')->delete($imagePath);
            }
            Equipment::where('uuid', $id)->delete();
            Reaction::where('equipment_uuid', $id)->delete();
            return response()->json([
                'messages' => 'Equipment was deleted successfully',
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
