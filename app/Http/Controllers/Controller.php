<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function createNotification(string $type, int $userToNotify, int $authorId, int $recipientId, string $post_uuid) {
        if ($type === config('notifications.post')) {
            Notification::create([
                'type' => $type,
                'post_uuid' => $post_uuid,
                'author_id' => $authorId,
                'user_id' => $userToNotify,
                'recipient_id' => $recipientId,
                'seen' => false
            ]);
        } else {
            Notification::updateOrCreate(
                [
                    'post_uuid' => $post_uuid,
                    'author_id' => $authorId,
                    'type' => $type,
                ],
                [
                    'user_id' => $userToNotify,
                    'recipient_id' => $recipientId,
                    'seen' => false
                ]
            );
        }
    }
}
