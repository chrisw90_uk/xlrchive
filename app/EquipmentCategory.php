<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentCategory extends Model
{
    protected $fillable = [
        'uuid', 'user_id', 'name', 'position'
    ];
    //
    protected $table = 'equipment_categories';

    public function equipment()
    {
        return $this->hasMany('App\Equipment','equipment_category_uuid', 'uuid');
    }
}
