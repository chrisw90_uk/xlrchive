<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewCommentOnForeignPost extends Mailable
{
    public $app_url;
    public $comment_author;
    public $original_post_author;
    public $recipient;

    public function __construct($original_post_author, $comment_author, $recipient)
    {
        $this->app_url = config('app.url');
        $this->comment_author = $comment_author;
        $this->original_post_author = $original_post_author;
        $this->recipient = $recipient;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('xlrchive@gmail.com')
            ->subject('Someone has commented on one of your posts')
            ->view('emails.new-foreign-comment');
    }
}
