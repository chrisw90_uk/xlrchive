<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewCommentOnProfile extends Mailable
{
    public $app_url;
    public $username;
    public $recipient_username;

    public function __construct($recipient_username, $username)
    {
        $this->app_url = config('app.url');
        $this->username = $username;
        $this->recipient_username = $recipient_username;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('xlrchive@gmail.com')
            ->subject('Someone has commented on a post on your profile')
            ->view('emails.new-comment');
    }
}
