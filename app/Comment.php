<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Comment extends Model
{
    protected $fillable = [
        'uuid',
        'author_id',
        'recipient_id',
        'post_author_id',
        'post_uuid',
        'content',
    ];
    public function post()
    {
        return $this->belongsTo('App\Post','post_uuid', 'uuid');
    }
    public function votes()
    {
        return $this->hasMany('App\Vote', 'comment_uuid', 'uuid' );
    }
    public function has_voted()
    {
        $user = Auth::user();
        return $this
            ->hasOne('App\Vote', 'comment_uuid', 'uuid' )
            ->where('user_id','=', $user['id']);
    }
}
