const path = require('path');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
    entry: ['./resources/website/js/discussion/app.js'],
    watch: true,
    mode: 'development',
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: 'node_modules'
    },
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    output: {
        filename: 'discussion.bundle.js',
        path: path.resolve(__dirname, 'public/js')
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: [
                    "url-loader"
                ]
            }
        ]
    },
    plugins: [
        new BrowserSyncPlugin(
            {
                host: '127.0.0.1',
                port: 3000,
                proxy: 'http://127.0.0.1:8000'
            },
        )
    ]
};
