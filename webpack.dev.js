const path = require('path');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
    entry: ['./resources/app/js/app.js'],
    watch: true,
    mode: 'development',
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: 'node_modules'
    },
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    output: {
        filename: 'app.bundle.js',
        path: path.resolve(__dirname, 'public/js')
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader", //outputs our CSS into a <style> tag in the document.
                    "css-loader", //parses the CSS into JavaScript and resolves any dependencies.
                    "sass-loader" //transforms Sass into CSS.
                ]
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    "url-loader"
                ]
            }
        ]
    },
    plugins: [
        new BrowserSyncPlugin(
            {
                host: '127.0.0.1',
                port: 3000,
                proxy: 'http://127.0.0.1:8000'
            },
        )
    ]
};
