const { src, dest, watch, series } = require("gulp");
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync').create();

function generateCSS(cb) {
    src('./resources/website/scss/site.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({debug: true}, (details) => {
            console.log(`${details.name}: ${details.stats.originalSize}`);
            console.log(`${details.name}: ${details.stats.minifiedSize}`);
        }))
        .pipe(dest('./public/css'));
    browserSync.reload();
    cb();
}

function minifyJS(cb) {
    src([
        './resources/website/js/site.js'
    ])
        .pipe(uglify())
        .pipe(dest('./public/js'));
    browserSync.reload();
    cb();
}

function initBrowserSync() {
    browserSync.init(null, {
        proxy: 'localhost:8000',
        port: 8000
    });
}

function defaultTask() {
    initBrowserSync();
    watchFiles();
}


function watchFiles() {
    watch('./resources/website/scss/**/*.scss', series(generateCSS));
    watch('./resources/website/js/**/*.js', minifyJS);
}

exports.default = defaultTask;
