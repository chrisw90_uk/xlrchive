import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { ToastsContainer, ToastsContainerPosition, ToastsStore } from 'react-toasts';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import store from '../../store';
import Dashboard from '../../dashboard/containers/Dashboard';
import Details from "../../details/containers/Details";
import Header from "../../header/containers/Header";
import Equipment from "../../equipment/containers/Equipment";
import Account from "../../account/containers/Account";
import Notifications from "../../notifications/containers/Notifications";

const App = () => (
    <Provider store={ store }>
        <Router history={createBrowserHistory()}>
            <Header />
            <Switch>
                <Route path="/dashboard" exact component={Dashboard} />
                <Route path="/dashboard/details" exact component={Details} />
                <Route path="/dashboard/equipment" exact component={Equipment} />
                <Route path="/dashboard/account" exact component={Account} />
                <Route path="/dashboard/notifications" exact component={Notifications} />
            </Switch>
        </Router>
        <ToastsContainer
            position={ToastsContainerPosition.BOTTOM_RIGHT}
            store={ToastsStore}
        />
    </Provider>
);

export default App;
