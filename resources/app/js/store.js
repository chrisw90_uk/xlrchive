import { composeWithDevTools } from 'redux-devtools-extension';
import { combineReducers, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { createBrowserHistory } from 'history';
import DetailsReducer from './details/reducers';
import EquipmentReducer from './equipment/reducers';
import AccountReducer from './account/reducers';

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    'details': DetailsReducer,
    'equipment': EquipmentReducer,
    'account': AccountReducer,
});

const composeEnhancers = composeWithDevTools({
    trace: true,
});

const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk))
);

export default store;
