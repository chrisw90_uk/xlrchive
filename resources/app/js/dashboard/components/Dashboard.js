import React from 'react';
import UserIcon from "../../icons/UserIcon";
import EquipmentIcon from "../../icons/EquipmentIcon";
import SettingsIcon from "../../icons/SettingsIcon";
import DashboardLink from "./DashboardLink";

const Dashboard = () => (
    <div className="dashboard full--w-footer bg--off-black">
        <div className="wrapper pad--x">
            <h1 className="text--center">Dashboard.</h1>
            <div className="dashboard__links flex flex--wrap pad--y">
                <DashboardLink
                    icon={<UserIcon strokeColor="white" />}
                    to="/dashboard/details"
                    text="Edit user details."
                />
                <DashboardLink
                    icon={<EquipmentIcon strokeColor="white" />}
                    to="/dashboard/equipment"
                    text="Edit equipment."
                />
                <DashboardLink
                    icon={<SettingsIcon strokeColor="white" />}
                    to="/dashboard/account"
                    text="Edit account settings."
                />
            </div>
        </div>
    </div>
);

export default Dashboard;
