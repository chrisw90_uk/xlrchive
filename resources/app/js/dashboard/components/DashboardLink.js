import React from 'react';
import { Link } from "react-router-dom";

export default ({
    icon,
    to,
    text,
}) => (
    <Link className="dashboard__link" to={to}>
        {icon}
        <span>
            {text}
        </span>
    </Link>
);
