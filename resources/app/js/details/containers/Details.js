import React from 'react';
import { connect } from 'react-redux'
import {
    fetchDetails,
} from "../actions";
import Details from '../components/Details';

const mapStateToProps = state => ({
    isFetching: state.details.isFetchingDetails,
    details: state.details.user,
});

const mapDispatchToProps = (dispatch) => ({
    fetchDetails: () => dispatch(fetchDetails()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Details);
