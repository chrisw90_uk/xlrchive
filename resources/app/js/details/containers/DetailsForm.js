import React from 'react';
import { connect } from 'react-redux'
import {
    saveDetails,
} from "../actions";
import DetailsForm from '../components/DetailsForm';

const mapStateToProps = state => ({
    isSaving: state.details.isSavingDetails,
    user: state.details.user,
});

const mapDispatchToProps = (dispatch) => ({
    saveDetails: (details) => dispatch(saveDetails(details)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DetailsForm);
