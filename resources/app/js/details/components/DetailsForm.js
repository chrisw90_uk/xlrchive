import React from 'react';
import ProfileImage from "./ProfileImage";
import DisplayImage from "./DisplayImage";
import Button from "../../components/Button";
import { USER_CATEGORIES } from "../constants";

class DetailsForm extends React.Component {

    state = {
        fetchedUser: {},
        user: {},
        isFetching: false,
        isSaving: false,
        profileImage: {
            currentImageUrl: null,
            imageToUpload: null,
            isImageRemoved: false,
        },
        displayImage: {
            currentImageUrl: null,
            imageToUpload: null,
            isImageRemoved: false,
        },
    };

    constructor(props) {
        super(props);
        this.imageRef = React.createRef();
    }

    componentDidMount() {
        const {
            user,
        } = this.props;
        this.populateState(user);
    }
    populateState = (user) => {
        this.setState({
            user,
            profileImage: {
                currentImageUrl: user.profile_image_url,
                imageToUpload: null,
                isImageRemoved: false,
            },
            displayImage: {
                currentImageUrl: user.display_image_url,
                imageToUpload: null,
                isImageRemoved: false,
            },
            fetchedUser: user,
        })
    };
    updateField = (field, value) => {
        this.setState(prevState => ({
            user: {
                ...prevState.user,
                [field]: value,
            }
        }))
    };
    updateUseAsDisplayName = (value) => {
        const { fetchedUser } = this.state;
        this.setState(prevState => ({
            user: {
                ...prevState.user,
                display_name: fetchedUser[value],
                use_as_display_name: prevState.user.use_as_display_name === value ? null : value,
            }
        }))
    };

    onProfileImageDone = (imageToUpload) => {
        this.setState(prevState => ({
            profileImage: {
                ...prevState.profileImage,
                imageToUpload,
            }
        }));
    };

    onProfileImageRemoved = () => {
        this.setState(prevState => ({
            profileImage: {
                ...prevState.profileImage,
                imageToUpload: null,
                isImageRemoved: true,
            }
        }));
    };

    onDisplayImageDone = (imageToUpload) => {
        this.setState(prevState => ({
            displayImage: {
                ...prevState.displayImage,
                imageToUpload,
            }
        }));
    };

    onDisplayImageRemoved = () => {
        this.setState(prevState => ({
            displayImage: {
                ...prevState.displayImage,
                imageToUpload: null,
                isImageRemoved: true,
            }
        }));
    };

    saveDetails = async (e) => {
        e.preventDefault();
        const {
            saveDetails,
        } = this.props;
        const { user, profileImage, displayImage } = this.state;
        saveDetails({ ...user, profileImage, displayImage });
    };

    render() {
        const {
            isSaving,
        } = this.props;
        const {
            user,
            profileImage,
            displayImage,
        } = this.state;
         return (
             <form className="form form--small" onSubmit={e => this.saveDetails(e)}>
                 <fieldset>
                     <legend className="text--center">Profile image.</legend>
                     <ProfileImage
                         currentImage={profileImage.currentImageUrl}
                         imageToUpload={profileImage.imageToUpload}
                         isImageRemoved={profileImage.isImageRemoved}
                         onDone={image => this.onProfileImageDone(image)}
                         onRemove={() => this.onProfileImageRemoved()}
                     />
                 </fieldset>
                 <fieldset>
                     <legend className="text--center">Header image.</legend>
                     <DisplayImage
                         currentImage={displayImage.currentImageUrl}
                         imageToUpload={displayImage.imageToUpload}
                         isImageRemoved={displayImage.isImageRemoved}
                         onDone={image => this.onDisplayImageDone(image)}
                         onRemove={() => this.onDisplayImageRemoved()}
                     />
                 </fieldset>
                 <fieldset>
                     <legend className="text--center">Display name.</legend>
                     <div className="form__field form__field--white">
                         <input
                             name="displayName"
                             type="text"
                             value={user.display_name || ''}
                             onChange={e => this.updateField('display_name', e.target.value)}
                             placeholder="Display name."
                             disabled={user.use_as_display_name !== null}
                         />
                         <label htmlFor="displayName">
                             Your display name will be displayed at the top of your profile.
                         </label>
                         <div className="form__field--sub">
                             <div className="form__field form__field--radio">
                                 <input
                                     id="useNameAsDisplayName"
                                     name="useNameAsDisplayName"
                                     type="checkbox"
                                     value="name"
                                     checked={user.use_as_display_name === 'name'}
                                     onChange={e => this.updateUseAsDisplayName(e.target.value)}
                                 />
                                 <label htmlFor="useNameAsDisplayName">
                                     Use name
                                 </label>
                             </div>
                             <div className="form__field form__field--radio">
                                 <input
                                     id="useUsernameAsDisplayName"
                                     name="useUsernameAsDisplayName"
                                     type="checkbox"
                                     value="username"
                                     checked={user.use_as_display_name === 'username'}
                                     onChange={e => this.updateUseAsDisplayName(e.target.value)}
                                 />
                                 <label htmlFor="useUsernameAsDisplayName">
                                     Use username
                                 </label>
                             </div>
                         </div>
                     </div>
                 </fieldset>
                 <fieldset>
                     <legend className="text--center">
                         Social.
                     </legend>
                     <div className="form__field form__field--white form__field--handle">
                         <label>
                             facebook.com/
                         </label>
                         <input
                             name="facebook"
                             type="text"
                             value={user.facebook || ''}
                             onChange={e => this.updateField('facebook', e.target.value)}
                         />
                     </div>
                     <div className="form__field form__field--white form__field--handle">
                         <label>
                             twitter.com/
                         </label>
                         <input
                             name="twitter"
                             type="text"
                             value={user.twitter || ''}
                             onChange={e => this.updateField('twitter', e.target.value)}
                         />
                     </div>
                     <div className="form__field form__field--white form__field--handle">
                         <label>
                             instagram.com/
                         </label>
                         <input
                             name="instagram"
                             type="text"
                             value={user.instagram || ''}
                             onChange={e => this.updateField('instagram', e.target.value)}
                         />
                     </div>
                     <div className="form__field form__field--white form__field--handle">
                         <label>
                             youtube.com/
                         </label>
                         <input
                             name="youtube"
                             type="text"
                             value={user.youtube || ''}
                             onChange={e => this.updateField('youtube', e.target.value)}
                         />
                     </div>
                 </fieldset>
                 <fieldset>
                     <legend className="text--center">Category.</legend>
                     <div className="form__field form__field--select form__field--white">
                         <select
                             name="category"
                             type="text"
                             value={user.category || ''}
                             onChange={e => this.updateField('category', e.target.value)}
                         >
                             <option value="">Select category.</option>
                             {Object.keys(USER_CATEGORIES).map(c => (
                                 <option value={c}>{USER_CATEGORIES[c]}.</option>
                             ))}
                         </select>
                         <label htmlFor="category">
                             Select the one that best describes what you do.
                             <br />We use this for various things, primarily to categorise you.
                         </label>
                     </div>
                 </fieldset>
                 <fieldset>
                     <legend className="text--center">Headline.</legend>
                     <div className="form__field form__field--white">
                         <input
                             name="headline"
                             type="text"
                             value={user.headline || ''}
                             onChange={e => this.updateField('headline', e.target.value)}
                             placeholder="Optional headline."
                         />
                         <label htmlFor="headline">
                             Your headline will appear on your profile underneath your display name.
                         </label>
                     </div>
                 </fieldset>
                 <fieldset>
                     <legend className="text--center">Bio.</legend>
                     <div className="form__field form__field--white">
                        <textarea
                            name="bio"
                            value={user.bio || ''}
                            onChange={e => this.updateField('bio', e.target.value)}
                            placeholder="Optional bio."
                        />
                         <label htmlFor="bio">
                             Your bio will appear under your display name and headline. Use this section
                             {' '}
                             to tell people more about what you do.
                         </label>
                     </div>
                 </fieldset>
                 <div className="text--right">
                     <Button
                         className="btn--outline btn--hover-white"
                         type="submit"
                         disabled={isSaving}
                         working={isSaving}
                         label="Save"
                     />
                 </div>
             </form>
         )
    }
}

export default DetailsForm;
