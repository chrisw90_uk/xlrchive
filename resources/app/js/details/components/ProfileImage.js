import React from 'react';
import ImageUploadButton from "../../components/ImageUploadButton";
import UserIcon from "../../icons/UserIcon";
import Modal from "../../components/Modal";
import ImageUploadAndCrop from "../../components/ImageUploadAndCrop";
import { convertCropToImageAsBase64, convertImageToBase64 } from "../../helpers";
import { IMAGE_TYPES } from "../../constants";
import DeleteIconButton from "../../components/DeleteIconButton";

class ProfileImage extends React.Component {
    state = {
        showModal: false,
        uploadedImage: null,
        isCroppingImage: false,
        error: '',
        crop: {
            width: 400,
            aspect: 1,
        },
    };
    constructor(props) {
        super(props);
        this.imageRef = React.createRef();
    }
    onImageUpload = async (files) => {
        const [file] = files;
        const img = new Image();
        img.src = window.URL.createObjectURL(file);
        img.onload = () => {
            if (img.naturalHeight < 350 || img.naturalWidth < 350) {
                return this.setState({
                    error: 'Your image is smaller than 350px by 350px'
                })
            }
            if (file.size >= 2000000) {
                return this.setState({
                    error: 'Your file is too large'
                })
            } else if (!IMAGE_TYPES.includes(file.type)) {
                return this.setState({
                    error: 'Your file must be a JPG or a PNG'
                })
            }
            convertImageToBase64(files[0]).then(uploadedImageAsBase64 => {
                this.setState({
                    uploadedImageAsBase64,
                    isCroppingImage: true,
                    error: '',
                })
            })
        };
    };
    cancelImageUpload = () => {
        this.setState({
            showModal: false,
            uploadedImage: null,
            isCroppingImage: false,
            crop: {},
        })
    };
    onFinishedCropping = () => {
        const {
            onDone
        } = this.props;
        const {
            uploadedImageAsBase64,
            crop,
        } = this.state;
        const imageToSave = convertCropToImageAsBase64(
            uploadedImageAsBase64, this.imageRef, crop, 350, 350,
        );
        onDone(imageToSave);
        this.setState({
            showModal: false,
        })
    };
    render() {
        const {
            showModal,
            isCroppingImage,
            crop,
            uploadedImageAsBase64,
            error
        } = this.state;
        const {
            currentImage,
            imageToUpload,
            onRemove,
            isImageRemoved,
        } = this.props;
        return (
            <div>
                {(imageToUpload || (currentImage && !isImageRemoved)) ? (
                    <div className="user__image">
                        <DeleteIconButton
                            onClick={onRemove}
                        />
                        <img src={imageToUpload || currentImage} alt="Profile image" />
                    </div>
                ) : (
                    <React.Fragment>
                        <ImageUploadButton
                            onClick={() => this.setState({ showModal: true })}
                            icon={<UserIcon strokeColor="white" />}
                            label="Add Image"
                        />
                        {showModal && (
                            <Modal
                                title="Add display image"
                                onCancel={() => this.cancelImageUpload()}
                                cancelAction="Cancel"
                                confirmAction="Done"
                                onConfirm={() => this.onFinishedCropping()}
                                disabledConfirmAction={!uploadedImageAsBase64}
                            >
                                <div ref={this.imageRef}>
                                    <ImageUploadAndCrop
                                        minWidth={400}
                                        croppingImage={isCroppingImage}
                                        crop={crop}
                                        uploadedImageAsBase64={uploadedImageAsBase64}
                                        onImageUpload={this.onImageUpload}
                                        onChangeCrop={crop => this.setState({ crop })}
                                    >
                                        <p>Your image must be a JPG or PNG at least 350px x 350px and less than 2MB in size.</p>
                                        {error.length > 0 && (
                                            <p className="text--red">{error}. Please try another file.</p>
                                        )}
                                    </ImageUploadAndCrop>
                                </div>
                            </Modal>
                        )}
                    </React.Fragment>
                )}
            </div>
        )
    }
}

export default ProfileImage;
