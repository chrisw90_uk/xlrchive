import React from 'react';
import ImageUploadButton from "../../components/ImageUploadButton";
import UserIcon from "../../icons/UserIcon";
import Modal from "../../components/Modal";
import { convertImageToBase64 } from "../../helpers";
import DeleteIconButton from "../../components/DeleteIconButton";
import ImageUploadWithoutCrop from "../../components/ImageUploadWithoutCrop";
import {IMAGE_TYPES} from "../../constants";
import ImageUploadAndCrop from "../../components/ImageUploadAndCrop";

class DisplayImage extends React.Component {
    state = {
        showModal: false,
        error: '',
    };
    onImageUpload = async (files) => {
        const { onDone } = this.props;
        const [file] = files;
        const img = new Image();
        img.src = window.URL.createObjectURL(file);
        img.onload = () => {
            if (img.naturalHeight < 720 || img.naturalWidth < 1280) {
                return this.setState({
                    error: 'Your image is smaller than 1280px by 720px'
                })
            }
            if (file.size >= 2000000) {
                return this.setState({
                    error: 'Your file is too large'
                })
            } else if (!IMAGE_TYPES.includes(file.type)) {
                return this.setState({
                    error: 'Your file must be a JPG or a PNG'
                })
            }
            convertImageToBase64(file).then(uploadedImageAsBase64 => {
                this.setState({ showModal: false, error: '' });
                onDone(uploadedImageAsBase64);
            })
        }
    };
    cancelImageUpload = () => {
        this.setState({
            showModal: false,
            uploadedImage: null,
        })
    };
    render() {
        const {
            showModal,
            error,
        } = this.state;
        const {
            currentImage,
            imageToUpload,
            onRemove,
            isImageRemoved,
        } = this.props;
        return (
            <div>
                {(imageToUpload || (currentImage && !isImageRemoved)) ? (
                    <div className="user__image-header">
                        <DeleteIconButton
                            onClick={onRemove}
                        />
                        <img src={imageToUpload || currentImage} alt="Display image" />
                    </div>
                ) : (
                    <React.Fragment>
                        <ImageUploadButton
                            onClick={() => this.setState({ showModal: true })}
                            icon={<UserIcon strokeColor="white" />}
                            label="Add Image"
                        />
                        {showModal && (
                            <Modal
                                title="Add display image"
                                cancelAction="Cancel"
                                onCancel={() => this.cancelImageUpload()}
                                hideConfirmAction
                            >
                                <ImageUploadWithoutCrop
                                    onImageUpload={this.onImageUpload}
                                >
                                    <p>Your image must be a JPG or PNG at least 1280px x 720px and less than 2MB in size.</p>
                                    {error.length > 0 && (
                                        <p className="text--red">{error}. Please try another file.</p>
                                    )}
                                </ImageUploadWithoutCrop>
                            </Modal>
                        )}
                    </React.Fragment>
                )}
            </div>
        )
    }
}

export default DisplayImage;
