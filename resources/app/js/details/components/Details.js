import React, { useEffect } from 'react';
import DetailsForm from "../containers/DetailsForm";

const Details = ({
    isFetching,
    fetchDetails,
    details,
}) => {
    useEffect(() => {
        fetchDetails();
    }, []);
    return (
        <div className="details flex justify--center full bg--red pad--y pad--x">
            <div className="text--center">
                <h1>User details.</h1>
                {isFetching ? (
                    <div>
                        <p>Loading...</p>
                    </div>
                ) : (
                    <React.Fragment>
                        {Object.keys(details).length > 0 && (
                            <DetailsForm />
                        )}
                    </React.Fragment>
                )}
            </div>
        </div>
    )
};

export default Details;
