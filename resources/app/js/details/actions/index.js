import axios from "axios";
import { ToastsStore } from 'react-toasts';

// Fetch Details

const fetchDetailsStart = () => ({
    type: 'DETAILS_FETCH_START',
});

const fetchDetailsSuccess = user => ({
    type: 'DETAILS_FETCH_SUCCESS',
    payload: {
        user,
    }
});

const fetchDetailsFail = errors => ({
    type: 'DETAILS_FETCH_FAIL',
    payload: {
        errors,
    }
});

export const fetchDetails = () => dispatch => {
    dispatch(fetchDetailsStart());
    axios.get('/api/user/details').then(res => {
        dispatch(fetchDetailsSuccess(res.data));
    }).catch(err => {
        dispatch(fetchDetailsFail(err.response.data));
        ToastsStore.error(err.response.data, 5000);
    });
};

// Save Details

const saveDetailsStart = () => ({
    type: 'DETAILS_SAVE_START',
});

const saveDetailsSuccess = user => ({
    type: 'DETAILS_SAVE_SUCCESS',
    payload: {
        user
    }
});

const saveDetailsFail = errors => ({
    type: 'DETAILS_SAVE_FAIL',
    payload: {
        errors,
    }
});

export const saveDetails = details => dispatch => {
    dispatch(saveDetailsStart());
    axios.put('/api/user/details', details).then(() => {
        dispatch(saveDetailsSuccess());
        ToastsStore.success("Details saved successfully", 5000);
    }).catch(err => {
        const { errors } = err.response.data;
        dispatch(saveDetailsFail(errors));
        ToastsStore.error(errors.join('\n'), 5000);
    });
};
