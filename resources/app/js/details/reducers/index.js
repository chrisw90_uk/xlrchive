const initialState = {
    isFetchingDetails: false,
    isSavingDetails: false,
    user: {}
};

const DetailsReducer = (state = initialState, action) => {
    switch(action.type){
        case 'DETAILS_FETCH_START':
            return {
                ...state,
                isFetchingDetails: true,
            };
        case 'DETAILS_FETCH_SUCCESS':
            return {
                ...state,
                isFetchingDetails: false,
                user: action.payload.user,
            };
        case 'DETAILS_FETCH_FAIL':
            return {
                ...state,
                isFetchingDetails: false,
            };
        case 'DETAILS_SAVE_START':
            return {
                ...state,
                isSavingDetails: true,
            };
        case 'DETAILS_SAVE_SUCCESS':
        case 'DETAILS_SAVE_FAIL':
            return {
                ...state,
                isSavingDetails: false,
            };
        default:
            return state;
    }
};

export default DetailsReducer;
