export const NOTIFICATION_TYPES = {
    POST: 'post',
    COMMENT: 'comment',
    COMMENT_FOREIGN: 'comment_foreign'
};

export const NOTIFICATION_GROUPS = {
    new: 'New',
    today: 'Today',
    yesterday: 'Yesterday',
    thisWeek: 'Last 7 Days',
    thisMonth: 'Last 28 Days',
};
