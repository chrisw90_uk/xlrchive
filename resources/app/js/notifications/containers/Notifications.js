import React from 'react';
import { connect } from 'react-redux'
import Notifications from '../components/Notifications';
import { getGroupedNotifications } from "../selectors";

const mapStateToProps = state => ({
    isFetchingNotifications: state.account.isFetchingNotifications,
    notifications: getGroupedNotifications(state),
    flatNotifications: state.account.flatNotifications,
});

export default connect(
    mapStateToProps,
    null
)(Notifications);
