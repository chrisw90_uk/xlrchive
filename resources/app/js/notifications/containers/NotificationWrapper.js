import React from 'react';
import { withRouter } from "react-router";
import { connect } from 'react-redux'
import NotificationWrapper from '../components/NotificationWrapper';
import { markNotificationsAsRead } from "../../account/actions";

const mapDispatchToProps = (dispatch) => ({
    markAsRead: ids => dispatch(markNotificationsAsRead(ids)),
});

export default withRouter(connect(
    null,
    mapDispatchToProps
)(NotificationWrapper));
