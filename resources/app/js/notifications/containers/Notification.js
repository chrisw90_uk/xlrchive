import React from 'react';
import { connect } from 'react-redux'
import Notification from '../components/Notification';
import { deleteNotification } from "../../account/actions";

const mapDispatchToProps = (dispatch) => ({
    deleteNotification: (id, group, seen) => dispatch(deleteNotification(id, group, seen))
});

export default connect(
    null,
    mapDispatchToProps
)(Notification);
