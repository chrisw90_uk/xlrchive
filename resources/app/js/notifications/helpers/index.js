import React from 'react';
import moment from 'moment';
import { NOTIFICATION_TYPES } from "../constants";
import PostNotification from "../components/PostNotification";
import CommentOnPostNotification from "../components/CommentOnPostNotification";
import CommentNotification from "../components/CommentNotification";

export const displayNotificationDate = date => {
    const dateAsMoment = moment(date);
    const today = moment().startOf('day');
    if (dateAsMoment.isSameOrAfter(today.subtract(1, 'day'))) {
        return dateAsMoment.format('HH:mm');
    }
    if (dateAsMoment.isBetween(today, today.subtract(7, 'day'))) {
        return dateAsMoment.format('dddd [at] HH:mm');
    }
    return dateAsMoment.format('D MMM [at] HH:mm');
};

export const renderNotification = (
    id, type, author, authorImage, recipient, date, preview, seen
) => {
    const props = { id, type, author, authorImage, recipient, date, preview, seen };
    switch (type) {
        case NOTIFICATION_TYPES.POST:
            return (
                <PostNotification {...props} />
            );
        case NOTIFICATION_TYPES.COMMENT:
            return (
                <CommentNotification {...props} />
            );
        case NOTIFICATION_TYPES.COMMENT_FOREIGN:
            return (
                <CommentOnPostNotification {...props} />
            );
        default:
            return '';
    }
};
