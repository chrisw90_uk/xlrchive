import React from 'react';
import NotificationsList from "./NotificationsList";
import { NOTIFICATION_GROUPS } from "../constants";

const Notifications = ({
    isFetchingNotifications,
    flatNotifications,
    notifications,
}) => (
    <div className="notifications full--w-footer bg--off-black pad--y pad--x">
        <div className="wrapper wrapper--small">
            <h1 className="text--center">
                Notifications.
            </h1>
            <div className="margin--t-xl">
                {isFetchingNotifications ? (
                    <p className="text--center">
                        Loading...
                    </p>
                ) : (
                    <React.Fragment>
                        {flatNotifications.length > 0 ? (
                            Object.keys(notifications).map(group => (
                                <React.Fragment key={group}>
                                    {notifications[group].length > 0 && (
                                        <React.Fragment>
                                            <h5 className="notifications__header">
                                                {NOTIFICATION_GROUPS[group]}
                                            </h5>
                                            <NotificationsList
                                                group={group}
                                                notifications={notifications[group].filter(n => !n.new)}
                                            />
                                        </React.Fragment>
                                    )}
                                </React.Fragment>
                            ))
                        ) : (
                            <p className="text--center">Much quiet.</p>
                        )}
                    </React.Fragment>
                )}
            </div>
        </div>
    </div>
);

export default Notifications;
