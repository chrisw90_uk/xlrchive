import React from 'react';
import { renderNotification } from "../helpers";
import DeleteIconButton from "../../components/DeleteIconButton";

const Notification = ({
    id,
    type,
    authorName,
    image,
    recipientName,
    date,
    preview,
    seen,
    group,
    deleteNotification,
}) => {
    return (
        <li className={`notifications__item${!seen ? ` notifications__item--new` : ' '}`}>
            {renderNotification(id, type, authorName, image, recipientName, date, preview, seen)}
            <a className="btn btn--icon btn--blue" href={`/user/${recipientName}`}>
                <i className="material-icons">
                    visibility
                </i>
            </a>
            <DeleteIconButton onClick={() => deleteNotification(id, group, seen !== 0)} />
        </li>
    );
};

export default Notification;
