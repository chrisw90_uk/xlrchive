import React from 'react';
import NotificationWrapper from '../containers/NotificationWrapper';

const PostNotification = ({
    author,
    ...rest
}) => (
    <NotificationWrapper
        icon="speaker_notes"
        imageAlt={author}
        {...rest}
    >
        {`${author} posted on your profile`}
    </NotificationWrapper>
);

export default PostNotification;
