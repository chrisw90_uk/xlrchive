import React from 'react';
import NotificationWrapper from '../containers/NotificationWrapper';

const CommentNotification = ({
    author,
    ...rest
}) => (
    <NotificationWrapper
        icon="comment"
        imageAlt={author}
        {...rest}
    >
        {`${author} commented on a post on your profile`}
    </NotificationWrapper>
);

export default CommentNotification;
