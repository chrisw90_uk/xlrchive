import React from 'react';
import { displayNotificationDate } from "../helpers";

const NotificationWrapper = ({
    id,
    authorImage,
    author,
    icon,
    children,
    date,
    preview,
    seen,
    markAsRead,
    recipient,
}) => (
    <button
        className="notifications__item-inner btn--reset flex align--center"
        onClick={() => !seen ? markAsRead([id]) : window.location.href = `/user/${recipient}`}
    >
        <div className="notifications__item-img">
            {authorImage ? <img src={authorImage} alt={author || ''} /> : <i className="material-icons">{icon}</i>}
        </div>
        <div className="notifications__item-content">
            {children}
            <div className="notifications__item-footer">
                {preview && (
                    <small>Post:
                        <em>
                            "{preview}"
                        </em>
                    </small>
                )}
                <small>{displayNotificationDate(date)}</small>
            </div>
        </div>
    </button>
);

export default NotificationWrapper;
