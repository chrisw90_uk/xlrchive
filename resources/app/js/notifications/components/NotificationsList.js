import React from 'react';
import Notification from "../containers/Notification";

const NotificationsList = ({
    group,
    notifications,
}) => (
    <ul className="notifications margin--b-md">
        {notifications.map(n => (
            <Notification
                key={n.id}
                id={n.id}
                type={n.type}
                authorName={n.author.username}
                image={n.author.profile_image_url}
                recipientName={n.recipient.username}
                date={n.date}
                preview={n.post?.post_preview}
                seen={n.seen}
                group={group}
            />
        ))}
    </ul>
);

export default NotificationsList;
