import React from 'react';
import NotificationWrapper from '../containers/NotificationWrapper';

const CommentOnPostNotification = ({
    author,
    recipient,
    ...rest
}) => (
    <NotificationWrapper
        author={author}
        recipient={recipient}
        icon="comment"
        {...rest}
    >
        {`${author} commented on your post on ${recipient === author ? 'their' : `${recipient}'s`} profile`}
    </NotificationWrapper>
);

export default CommentOnPostNotification;
