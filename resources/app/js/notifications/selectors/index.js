import { createSelector } from 'reselect';
import moment from "moment";

const getNotifications = state => state.account.flatNotifications;

export const getGroupedNotifications = createSelector(
    [getNotifications],
    (notifications) => {
        const todayDate = moment().startOf('day');
        const yesterdayDate = moment().subtract(1, 'days').startOf('day');
        const thisWeekDate = moment().subtract(7, 'days').startOf('day');
        const thisMonthDate = moment().subtract(28, 'days').startOf('day');
        return {
            today: notifications.filter(n => moment(n.date).isAfter(todayDate)),
            yesterday: notifications.filter(n => moment(n.date).isBetween(yesterdayDate, todayDate)),
            thisWeek: notifications.filter(n => moment(n.date).isBetween(thisWeekDate, yesterdayDate)),
            thisMonth: notifications.filter(n => moment(n.date).isBetween(thisMonthDate, thisWeekDate)),
        }
    }
);
