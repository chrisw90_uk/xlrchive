export const convertImageToBase64 = (file) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
};

export const convertCropToImageAsBase64 = (imageAsBase64, imageRef, crop, height = null, width = null) => {
    const image = new Image();
    image.src = imageAsBase64;
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / imageRef.current.getBoundingClientRect().width;
    const scaleY = image.naturalHeight / imageRef.current.getBoundingClientRect().height;
    canvas.width = width || crop.width;
    canvas.height = height || crop.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(
        image,
        crop.x * scaleX,
        crop.y * scaleY,
        crop.width * scaleX,
        crop.height * scaleY,
        0,
        0,
        width || crop.width,
        height || crop.height,
    );
    return canvas.toDataURL('image/jpeg');
};
