import React from 'react';
import { connect } from 'react-redux'
import { setDeleteModalParams, addEquipment, editEquipment } from "../actions";
import EquipmentItem from '../components/EquipmentItem';

const mapStateToProps = state => ({
    isSavingNewEquipmentIn: state.equipment.isSavingNewEquipmentIn,
    isSavingEquipment: state.equipment.isSavingEquipment,
});

const mapDispatchToProps = (dispatch) => ({
    setDeleteModalParams: (toggle, type, id, name, parentId) => dispatch(
        setDeleteModalParams(toggle, type, id, name, parentId)
    ),
    addEquipment: (equipment, position) => dispatch(addEquipment(equipment, position)),
    editEquipment: (equipment) => dispatch(editEquipment(equipment))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EquipmentItem);
