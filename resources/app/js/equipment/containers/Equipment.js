import React from 'react';
import { connect } from 'react-redux'
import {
    fetchEquipment,
    addCategory,
    setDeleteModalParams,
    deleteCategory,
    deleteEquipment,
    toggleReactions,
} from "../actions";
import Equipment from '../components/Equipment';

const mapStateToProps = state => ({
    isFetching: state.equipment.isFetchingEquipment,
    enableReactions: state.equipment.enableReactions,
    equipment: state.equipment.equipment,
    isDeleting: state.equipment.isDeleting,
    toDelete: state.equipment.toDelete,
    isSavingNewCategory: state.equipment.isSavingNewCategory,
});

const mapDispatchToProps = (dispatch) => ({
    fetchEquipment: () => dispatch(fetchEquipment()),
    addCategory: (name, position) => dispatch(addCategory(name, position)),
    setDeleteModalParams: (toggle, type, id, name) => dispatch(setDeleteModalParams(toggle, type, id, name)),
    deleteCategory: (id) => dispatch(deleteCategory(id)),
    deleteEquipment: (categoryId, id) => dispatch(deleteEquipment(categoryId, id)),
    toggleReactions: (toggle) => dispatch(toggleReactions(toggle)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Equipment);
