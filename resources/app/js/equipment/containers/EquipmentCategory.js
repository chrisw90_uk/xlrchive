import React from 'react';
import { connect } from 'react-redux'
import { setDeleteModalParams, editCategory } from "../actions";
import EquipmentCategory from '../components/EquipmentCategory';

const mapStateToProps = state => ({
    isSavingCategory: state.equipment.isSavingCategory,
});

const mapDispatchToProps = (dispatch) => ({
    setDeleteModalParams: (toggle, type, id, name) => dispatch(setDeleteModalParams(toggle, type, id, name)),
    editCategory: (id, name) => dispatch(editCategory(id, name)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EquipmentCategory);
