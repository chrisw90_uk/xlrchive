import React from 'react';
import EquipmentCategory from "../containers/EquipmentCategory";
import Button from "../../components/Button";
import Modal from "../../components/Modal";
import 'react-image-crop/lib/ReactCrop.scss';

class Equipment extends React.Component {
    state = {
        isFetching: false,
        isSaving: false,
        newCategoryName: '',
        categories: [],
        errors: []
    };

    componentDidMount() {
        const { fetchEquipment } = this.props;
        fetchEquipment();
    }

    addNewCategory = async (e, name, position) => {
        const { addCategory } = this.props;
        e.preventDefault();
        const success = await addCategory(name, position);
        if (success) {
            this.setState(({
                newCategoryName: '',
            }));
        }
    };

    render() {
        const {
            newCategoryName,
        } = this.state;
        const {
            isFetching,
            equipment,
            enableReactions,
            isDeleting,
            toDelete,
            setDeleteModalParams,
            deleteCategory,
            deleteEquipment,
            toggleReactions,
            isSavingNewCategory,
        } = this.props;
        return (
            <div className="equipment full bg--off-black pad--y pad--x">
                <div className="wrapper">
                    <h1 className="text--center">
                        Equipment.
                    </h1>
                    <div className="margin--t-xl">
                        <div className="equipment__copy wrapper wrapper--small margin--b-xl">
                            <p>
                                Use this section to show off your gear!
                            </p>
                            <p>
                                Firstly, create some categories to organise your equipment. Once you've done that, you can
                                {' '}
                                get started adding your gear. Simply specify a name, a primary and secondary category,
                                {' '}
                                and upload an optional image and it will automatically show on your profile.
                            </p>
                            {isFetching ? null : (
                                <React.Fragment>
                                    <hr />
                                    <form className="form">
                                        <div className="form__field form__field--radio margin--b-none">
                                            <input
                                                id="allowReactions"
                                                name="allowReactions"
                                                type="checkbox"
                                                value="name"
                                                checked={enableReactions === null ? true : enableReactions}
                                                onChange={() => toggleReactions(enableReactions === null ? false : !enableReactions)}
                                            />
                                            <label htmlFor="allowReactions">
                                                Enable reactions
                                            </label>
                                        </div>
                                        <label>Allow other users to react to your awesome gear.</label>
                                    </form>
                                </React.Fragment>
                            )}
                        </div>
                        {isFetching ? (
                            <div className="text--center">
                                <p>Loading...</p>
                            </div>
                        ) : (
                            <React.Fragment>
                                {(toDelete.type === 'equipment-category' && toDelete.showModal) && (
                                    <Modal
                                        title="Are you sure?"
                                        onCancel={() => setDeleteModalParams()}
                                        cancelAction="Cancel"
                                        onConfirm={() => deleteCategory(toDelete.id)}
                                        confirmAction="Delete"
                                        working={isDeleting}
                                    >
                                        <p>This will delete the equipment category, <strong>'{toDelete.name}'</strong></p>
                                        <p>All equipment within this category will also be deleted.</p>
                                        <p>This action cannot be undone.</p>
                                    </Modal>
                                )}
                                {(toDelete.type === 'equipment' && toDelete.showModal) && (
                                    <Modal
                                        title="Are you sure?"
                                        onCancel={() => setDeleteModalParams()}
                                        cancelAction="Cancel"
                                        onConfirm={() => deleteEquipment(toDelete.parentId, toDelete.id)}
                                        confirmAction="Delete"
                                        working={isDeleting}
                                    >
                                        <p>This will delete the equipment item, <strong>'{toDelete.name}'</strong></p>
                                        <p>This action cannot be undone.</p>
                                    </Modal>
                                )}
                                <ul className="user__equipment-categories">
                                    {equipment.map(c => (
                                        <EquipmentCategory
                                            key={c.uuid}
                                            id={c.uuid}
                                            name={c.name}
                                            equipment={c.equipment}
                                            onDelete={setDeleteModalParams}
                                        />
                                    ))}
                                    <li className="user__equipment-categories-item equipment__category--create">
                                        <form
                                            className="form"
                                            onSubmit={e => this.addNewCategory(
                                                e, newCategoryName, equipment.length + 1
                                            )}
                                        >
                                            <fieldset>
                                                <h6>
                                                    New category.
                                                </h6>
                                                <div className="form__field">
                                                    <input
                                                        type="text"
                                                        placeholder="Name."
                                                        value={newCategoryName}
                                                        onChange={e => this.setState({ newCategoryName: e.target.value })}
                                                    />
                                                </div>
                                                <div className="text--right">
                                                    <Button
                                                        type="submit"
                                                        className="btn btn--outline btn--hover-red"
                                                        disabled={!newCategoryName || isSavingNewCategory}
                                                        working={isSavingNewCategory}
                                                        label="Add"
                                                    />
                                                </div>
                                            </fieldset>
                                        </form>
                                    </li>
                                </ul>
                            </React.Fragment>
                        )}
                    </div>
                </div>
            </div>
        )
    }
};

export default Equipment;
