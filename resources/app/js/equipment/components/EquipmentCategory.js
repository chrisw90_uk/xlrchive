import React from 'react';
import axios from "axios";
import EquipmentItem from "../containers/EquipmentItem";
import Button from "../../components/Button";
import DeleteIconButton from "../../components/DeleteIconButton";

class EquipmentCategory extends React.Component {
    state = {
        name: '',
        changes: false,
    };

    componentDidMount() {
        const {
            name
        } = this.props;
        this.setState({
            name
        });
    }

    updateCategoryName = (name) => {
        const { name: originalName } = this.props;
        this.setState({
            name,
            changes: originalName !== name,
        })
    };

    saveCategoryChanges = async (e) => {
        e.preventDefault();
        const { id, editCategory } = this.props;
        const { name } = this.state;
        const changed = await editCategory(id, name);
        if (changed) {
            this.setState( {
                changes: false,
            });
        }
    };

    saveNewEquipment = (e, equipment) => {
        e.preventDefault();
        const { equipment: equipmentList } = this.state;
        this.setState({
            isSavingNewEquipment: true,
        });
        return axios.post(
            '/api/equipment',
            { ...equipment, position: equipmentList.length + 1 }
        ).then(res => {
            this.setState(prevState => ({
                isSavingNewEquipment: false,
                equipment: [
                    ...prevState.equipment,
                    res.data
                ],
            }));
        }).catch(() => {
            this.setState({
                errors: ['There was a problem saving your equipment. Please try again.'],
                isSavingNewEquipment: false
            });
            return false;
        })
    };

    saveEquipmentChanges = (e, equipment) => {
        e.preventDefault();
        this.setState({
            isSavingEquipmentChangesTo: equipment.id,
        });
        return axios.put(
            '/api/equipment',
            equipment,
        ).then(() => {
            this.setState({
                isSavingEquipmentChangesTo: null,
            });
            return true;
        }).catch(() => {
            this.setState({
                errors: ['There was a problem saving your changes. Please try again.'],
                isSavingEquipmentChangesTo: null
            });
            return false;
        })
    };

    render() {
        const {
            id,
            equipment,
            isSavingCategory,
            setDeleteModalParams,
        } = this.props;
        const {
            name,
            changes,
        } = this.state;
        return (
            <li className="user__equipment-categories-item equipment__category--edit">
                <form className="form">
                    <div className="form__field">
                        <input
                            type="text"
                            value={name}
                            onChange={e => this.updateCategoryName(e.target.value)}
                        />
                    </div>
                    {changes && (
                        <Button
                            type="submit"
                            className="btn--outline btn--hover-red"
                            working={isSavingCategory === id}
                            label="Save"
                            disabled={isSavingCategory !== null || !name}
                            onClick={e => this.saveCategoryChanges(e)}
                        />
                    )}
                    <DeleteIconButton
                        onClick={() => setDeleteModalParams(true, 'equipment-category', id, name)}
                    />
                </form>
                <ul>
                    {equipment.map(e => (
                        <EquipmentItem
                            key={e.id}
                            mode="edit"
                            equipmentCategoryId={id}
                            id={e.uuid}
                            name={e.name}
                            imageUrl={e.image_url}
                            primaryCategory={e.primary_category}
                            secondaryCategory={e.secondary_category}
                        />
                    ))}
                    <EquipmentItem
                        mode="new"
                        equipmentCategoryId={id}
                        position={equipment.length + 1}
                    />
                </ul>
            </li>
        )
    }
}

export default EquipmentCategory;
