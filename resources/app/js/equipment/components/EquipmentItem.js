import React from 'react';
import Button from '../../components/Button';
import { CATEGORIES } from "../constants";
import EquipmentIcon from "../../icons/EquipmentIcon";
import CategoryDropdown from "./CategoryDropdown";
import DeleteIconButton from "../../components/DeleteIconButton";
import Modal from "../../components/Modal";
import ImageUploadAndCrop from "../../components/ImageUploadAndCrop";
import { convertImageToBase64, convertCropToImageAsBase64 } from "../../helpers";
import ImageUploadButton from "../../components/ImageUploadButton"
import {IMAGE_TYPES} from "../../constants";

class EquipmentItem extends React.Component {
    state = {
        mode: '',
        name: '',
        primaryCategory: '',
        secondaryCategory: '',
        imageUrl: '',
        isImageRemoved: false,
        showImageUploadModal: false,
        uploadedImage: null,
        imageToUpload: '',
        croppingImage: false,
        error: '',
        crop: {
            aspect: 1,
            height: 200,
            width: 200,
        },
        changes: false,
    };

    constructor(props) {
        super(props);
        this.imageRef = React.createRef();
    }

    componentDidMount() {
        const {
            name,
            primaryCategory,
            secondaryCategory,
            imageUrl,
        } = this.props;
        this.setState({
            name: name || '',
            primaryCategory: primaryCategory || '',
            secondaryCategory: secondaryCategory || '',
            imageUrl,
        });
    }

    updateField = (field, value) => {
        const { mode } = this.props;
        this.setState({
            [field]: value,
        });
        if (field === 'primaryCategory') {
            this.setState({
                secondaryCategory: '',
            })
        }
        if (mode === 'edit') {
            const { name, primaryCategory, secondaryCategory } = this.state;
            const currentValues = {
                name,
                primaryCategory,
                secondaryCategory,
                [field]: value,
            };
            this.checkForChanges(currentValues)
        }
    };

    checkForChanges = (currentValues) => {
        const changes = Object.keys(currentValues).some(val => currentValues[val] !== this.props[val]);
        this.setState({ changes });
    };

    resetFields = () => {
        this.setState({
            name: '',
            primaryCategory: '',
            secondaryCategory: '',
            imageToUpload: '',
            isImageRemoved: false,
            uploadedImage: null,
            croppingImage: false,
        })
    };

    resetImageUpload = () => {
        this.setState({
            imageToUpload: '',
            uploadedImage: null,
            croppingImage: false,
            showImageUploadModal: false,
        })
    };


    onImageUpload = async (files) => {
        const [file] = files;
        const img = new Image();
        img.src = window.URL.createObjectURL(file);
        img.onload = () => {
            if (img.naturalHeight < 200 || img.naturalWidth < 200) {
                return this.setState({
                    error: 'Your image is smaller than 200px by 200px'
                })
            }
            if (file.size >= 2000000) {
                return this.setState({
                    error: 'Your file is too large'
                })
            } else if (!IMAGE_TYPES.includes(file.type)) {
                return this.setState({
                    error: 'Your file must be a JPG or a PNG'
                })
            }
            convertImageToBase64(file).then(uploadedImageAsBase64 => {
                this.setState({
                    uploadedImage: file,
                    uploadedImageAsBase64,
                    croppingImage: true,
                    error: '',
                });
            })
        }
    };

    onUpdateCrop = (crop) => {
        this.setState({
            crop,
        })
    };

    onImageSave = () => {
        const { uploadedImageAsBase64, crop } = this.state;
        const imageToUpload = convertCropToImageAsBase64(
            uploadedImageAsBase64, this.imageRef, crop, 250, 250
        );
        this.setState({
            imageToUpload,
            showImageUploadModal: false,
            changes: true,
        });
    };

    removeImage = () => {
        this.setState({
            isImageRemoved: true,
            imageToUpload: '',
            uploadedImage: null,
            croppingImage: false,
            changes: true,
        });
    };

    resetChanges = () => {
        const {
            name,
            primaryCategory,
            secondaryCategory,
            imageUrl,
        } = this.props;
        this.setState({
            name,
            primaryCategory,
            secondaryCategory,
            imageUrl,
            changes: false,
            isImageRemoved: false,
        });
        this.resetImageUpload()
    };

    render() {
        const {
            mode,
            equipmentCategoryId,
            id,
            position,
            setDeleteModalParams,
            addEquipment,
            isSavingNewEquipmentIn,
            editEquipment,
            isSavingEquipment,
        } = this.props;
        const {
            name,
            primaryCategory,
            secondaryCategory,
            changes,
            showImageUploadModal,
            imageUrl,
            isImageRemoved,
            uploadedImage,
            uploadedImageAsBase64,
            imageToUpload,
            croppingImage,
            crop,
            error,
        } = this.state;
        const imageToShow = imageToUpload || (imageUrl && !isImageRemoved);
        return (
            <li
                className={
                    `user__equipment-list-item equipment__item--edit ${mode === 'new' ? 'equipment__item--new' : ''}`
                }
            >
                {showImageUploadModal && (
                    <Modal
                        title={`${imageUrl ? 'Edit' : 'Add'} equipment image.`}
                        onCancel={() => this.resetImageUpload()}
                        cancelAction="Cancel"
                        confirmAction="Save"
                        onConfirm={() => this.onImageSave()}
                        disabledConfirmAction={uploadedImage !== null}
                    >
                        <div className="text--center">
                            <div className="image-upload__bounding" ref={this.imageRef}>
                                <ImageUploadAndCrop
                                    croppingImage={croppingImage}
                                    crop={crop}
                                    uploadedImageAsBase64={uploadedImageAsBase64}
                                    onImageUpload={this.onImageUpload}
                                    onChangeCrop={this.onUpdateCrop}
                                    minWidth={200}
                                >
                                    <p>Your image must be a JPG or PNG at least 200px x 200px and less than 2MB in size.</p>
                                    {error.length > 0 && (
                                        <p className="text--red">{error}. Please try another file.</p>
                                    )}
                                </ImageUploadAndCrop>
                            </div>
                        </div>
                    </Modal>
                )}
                {mode === 'new' && (
                    <h6>Add new equipment.</h6>
                )}
                <form className="form">
                    <div className="flex flex--wrap justify--center form__inner">
                        <div className="user__equipment-image equipment__item--image">
                            {imageToShow ? (
                                <React.Fragment>
                                    <DeleteIconButton
                                        onClick={() => this.removeImage()}
                                    />
                                    <img
                                        alt={name}
                                        src={
                                            imageToUpload || (!isImageRemoved && imageUrl) && imageUrl
                                        }/>
                                </React.Fragment>
                            ) : (
                                <ImageUploadButton
                                    onClick={() => this.setState({ showImageUploadModal: true })}
                                    icon={<EquipmentIcon strokeColor="white" />}
                                    label="Add image"
                                />
                            )}
                        </div>
                        <div className="user__equipment-details">
                            <div className="form__field">
                                <input
                                    type="text"
                                    value={name}
                                    onChange={e => this.updateField('name', e.target.value)}
                                    placeholder="Name of equipment."
                                />
                                {mode === 'edit' && (
                                    <DeleteIconButton
                                        onClick={() => setDeleteModalParams(true, 'equipment', id, name, equipmentCategoryId)}
                                    />
                                )}
                            </div>
                            <div className="flex equipment__item--categories">
                                <CategoryDropdown
                                    label={primaryCategory ? CATEGORIES[primaryCategory].title : 'Select category.'}
                                    field="primaryCategory"
                                    onChange={this.updateField}
                                    options={Object.keys(CATEGORIES).reduce((acc, c) => (
                                        { ...acc, [c]: CATEGORIES[c].title}
                                    ), {})}
                                />
                                <span> > </span>
                                <CategoryDropdown
                                    disabled={!primaryCategory}
                                    label={(primaryCategory && secondaryCategory)
                                        ? CATEGORIES[primaryCategory].subCategories[secondaryCategory]
                                        : 'Select subcategory.'
                                    }
                                    field="secondaryCategory"
                                    onChange={this.updateField}
                                    options={primaryCategory ? CATEGORIES[primaryCategory].subCategories : {}}
                                />
                            </div>
                        </div>
                    </div>
                    {mode === 'new' ? (
                        <div className="text--right">
                            <Button
                                disabled={
                                    isSavingNewEquipmentIn === equipmentCategoryId
                                    || (!name || !primaryCategory || !secondaryCategory)
                                }
                                className="btn btn--outline btn--hover-red"
                                type="submit"
                                working={isSavingNewEquipmentIn === equipmentCategoryId}
                                label="Add"
                                onClick={async e => {
                                    e.preventDefault();
                                    const created = await addEquipment(
                                        {
                                            equipmentCategoryId,
                                            name,
                                            imageToUpload,
                                            primaryCategory,
                                            secondaryCategory
                                        },
                                        position
                                    );
                                    if (created) this.resetFields();
                                }}
                            />
                        </div>
                    ) : (
                        <React.Fragment>
                            {changes && (
                                <div className="text--right equipment__item--actions">
                                    <Button
                                        className="btn--outline btn--hover-red"
                                        type="button"
                                        label="Reset"
                                        disabled={isSavingEquipment !== null}
                                        onClick={() => this.resetChanges()}
                                    />
                                    <Button
                                        className="btn--outline btn--hover-red"
                                        type="submit"
                                        label="Save"
                                        working={isSavingEquipment === id}
                                        disabled={
                                            isSavingEquipment !== null
                                            || (!name || !primaryCategory || !secondaryCategory)
                                        }
                                        onClick={async e => {
                                            e.preventDefault();
                                            const { updated, imageUrl: newImage } = await editEquipment(
                                                {
                                                    equipmentCategoryId,
                                                    id,
                                                    name,
                                                    imageUrl,
                                                    imageToUpload,
                                                    isImageRemoved,
                                                    primaryCategory,
                                                    secondaryCategory
                                                }
                                            );
                                            if (updated) {
                                                this.setState({
                                                    changes: false,
                                                    imageUrl: newImage,
                                                    isImageRemoved: false,
                                                });
                                                this.resetImageUpload();
                                            }
                                        }}
                                    />
                                </div>
                            )}
                        </React.Fragment>
                    )}
                </form>
            </li>
        )
    }
}

export default EquipmentItem;
