import React, { useState } from 'react';
import {
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle
} from "reactstrap";

const CategoryDropdown = ({
    label,
    options,
    field,
    onChange,
    disabled
}) => {
    const [isOpen, toggleIsOpen] = useState(false);
    return (
        <Dropdown
            disabled={disabled || false}
            isOpen={isOpen}
            toggle={() => toggleIsOpen(!isOpen)}
        >
            <DropdownToggle className="text--muted">
                {label}
                <i className="material-icons">
                    {isOpen ? 'expand_less' : 'expand_more'}
                </i>
            </DropdownToggle>
            <DropdownMenu>
                {Object.keys(options).map(c => (
                    <DropdownItem
                        key={c}
                        onClick={() => onChange(field, c)}
                    >
                        {options[c]}
                    </DropdownItem>
                ))}
            </DropdownMenu>
        </Dropdown>
    )
};

export default CategoryDropdown;
