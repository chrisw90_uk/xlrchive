import axios from "axios";
import { ToastsStore } from 'react-toasts';

// Fetch equipment categories and equipment

const fetchEquipmentStart = () => ({
    type: 'EQUIPMENT_FETCH_START',
});

const fetchEquipmentSuccess = (equipment, enableReactions) => ({
    type: 'EQUIPMENT_FETCH_SUCCESS',
    payload: {
        equipment,
        enableReactions,
    }
});

const fetchEquipmentFail = errors => ({
    type: 'EQUIPMENT_FETCH_FAIL',
    payload: {
        errors,
    }
});

export const fetchEquipment = () => dispatch => {
    dispatch(fetchEquipmentStart());
    axios.get('/api/equipment-categories').then(res => {
        const { equipment, enable_reactions: enableReactions } = res.data;
        dispatch(fetchEquipmentSuccess(equipment, enableReactions));
    }).catch(err => {
        dispatch(fetchEquipmentFail(err.response.data));
        ToastsStore.error(err.response.data, 5000);
    });
};

// Add new category

const addCategoryStart = () => ({
    type: 'EQUIPMENT_CATEGORY_ADD_START',
});

const addCategorySuccess = category => ({
    type: 'EQUIPMENT_CATEGORY_ADD_SUCCESS',
    payload: {
        category,
    }
});

const addCategoryFail = errors => ({
    type: 'EQUIPMENT_CATEGORY_ADD_FAIL',
    payload: {
        errors,
    }
});

export const addCategory = (name, position) => dispatch => {
    dispatch(addCategoryStart());
    return axios.post('/api/equipment-categories', { name, position }).then(res => {
        dispatch(addCategorySuccess(res.data));
        ToastsStore.success("Category added successfully", 5000);
        return true;
    }).catch(err => {
        dispatch(addCategoryFail(err.response.data));
        ToastsStore.error(err.response.data, 5000);
        return false;
    });
};

// Edit category

const editCategoryStart = id => ({
    type: 'EQUIPMENT_CATEGORY_EDIT_START',
    payload: {
        id
    }
});

const editCategorySuccess = (id, name) => ({
    type: 'EQUIPMENT_CATEGORY_EDIT_SUCCESS',
    payload: {
        id,
        name,
    }
});

const editCategoryFail = errors => ({
    type: 'EQUIPMENT_CATEGORY_EDIT_FAIL',
    payload: {
        errors,
    }
});

export const editCategory = (id, name) => dispatch => {
    dispatch(editCategoryStart(id));
    return axios.put('/api/equipment-categories', { id, name }).then(res => {
        dispatch(editCategorySuccess(id, name));
        ToastsStore.success("Category edited successfully", 5000);
        return true;
    }).catch(err => {
        dispatch(editCategoryFail(err.response.data));
        ToastsStore.error(err.response.data, 5000);
        return false;
    });
};

// Delete category

export const setDeleteModalParams = (toggle = false, type = '', id = '', name = '', parentId = '') => ({
    type: 'EQUIPMENT_DELETE_MODAL_SET_PARAMS',
    payload: {
        toggle,
        type,
        id,
        name,
        parentId,
    }
});

const deleteCategoryStart = () => ({
    type: 'EQUIPMENT_CATEGORY_DELETE_START',
});

const deleteCategorySuccess = id => ({
    type: 'EQUIPMENT_CATEGORY_DELETE_SUCCESS',
    payload: {
        id,
    }
});

const deleteCategoryFail = errors => ({
    type: 'EQUIPMENT_CATEGORY_DELETE_FAIL',
    payload: {
        errors,
    }
});

export const deleteCategory = (id) => dispatch => {
    dispatch(deleteCategoryStart());
    return axios.delete(`/api/equipment-categories?id=${id}`).then(res => {
        dispatch(deleteCategorySuccess(id));
        ToastsStore.success("Category deleted successfully", 5000);
        return dispatch(setDeleteModalParams());
    }).catch(err => {
        ToastsStore.error(err.response.data, 5000);
        return dispatch(deleteCategoryFail(err.response.data));
    });
};

// Add Equipment

const addEquipmentStart = (categoryId) => ({
    type: 'EQUIPMENT_ADD_START',
    payload: {
        categoryId
    }
});

const addEquipmentSuccess = (categoryId, equipment) => ({
    type: 'EQUIPMENT_ADD_SUCCESS',
    payload: {
        categoryId,
        equipment,
    }
});

const addEquipmentFail = errors => ({
    type: 'EQUIPMENT_ADD_FAIL',
    payload: {
        errors,
    }
});

export const addEquipment = (equipment, position) => dispatch => {
    dispatch(addEquipmentStart(equipment.equipmentCategoryId));
    return axios.post('/api/equipment', { ...equipment, position }).then(res => {
        dispatch(addEquipmentSuccess(equipment.equipmentCategoryId, res.data));
        ToastsStore.success("Equipment added successfully", 5000);
        return true;
    }).catch(err => {
        dispatch(addEquipmentFail(err.response.data));
        ToastsStore.error(err.response.data, 5000);
        return false;
    });
};

// Edit Equipment

const editEquipmentStart = (id) => ({
    type: 'EQUIPMENT_EDIT_START',
    payload: {
        id
    }
});

const editEquipmentSuccess = (categoryId, equipment) => ({
    type: 'EQUIPMENT_EDIT_SUCCESS',
    payload: {
        categoryId,
        equipment,
    }
});

const editEquipmentFail = errors => ({
    type: 'EQUIPMENT_EDIT_FAIL',
    payload: {
        errors,
    }
});

export const editEquipment = (equipment) => dispatch => {
    dispatch(editEquipmentStart(equipment.id));
    return axios.put('/api/equipment', equipment).then(res => {
        const { updated, imageUrl } = res.data;
        dispatch(editEquipmentSuccess(equipment.equipmentCategoryId, equipment));
        ToastsStore.success("Equipment edited successfully", 5000);
        return {
            updated,
            imageUrl,
        };
    }).catch(err => {
        const { errors } = err.response.data;
        dispatch(editEquipmentFail(errors));
        ToastsStore.error(errors.join('\n'), 5000);
        return false;
    });
};

// Delete Equipment

const deleteEquipmentStart = () => ({
    type: 'EQUIPMENT_DELETE_START',
});

const deleteEquipmentSuccess = (categoryId, id) => ({
    type: 'EQUIPMENT_DELETE_SUCCESS',
    payload: {
        categoryId,
        id,
    }
});

const deleteEquipmentFail = errors => ({
    type: 'EQUIPMENT_DELETE_FAIL',
    payload: {
        errors,
    }
});

export const deleteEquipment = (equipmentId, id) => dispatch => {
    dispatch(deleteEquipmentStart());
    return axios.delete(`/api/equipment?id=${id}`).then(() => {
        dispatch(deleteEquipmentSuccess(equipmentId, id));
        ToastsStore.success("Equipment deleted successfully", 5000);
        return dispatch(setDeleteModalParams());
    }).catch(err => {
        ToastsStore.error(err.response.data, 5000);
        return dispatch(deleteEquipmentFail(err.response.data));
    });
};


// Toggle reactions

const toggleReactionsStart = toggle => ({
    type: 'EQUIPMENT_TOGGLE_REACTIONS_START',
    payload: {
        toggle,
    }
});

const toggleReactionsSuccess = () => ({
    type: 'EQUIPMENT_TOGGLE_REACTIONS_SUCCESS',
});

const toggleReactionsFail = toggle => ({
    type: 'EQUIPMENT_TOGGLE_REACTIONS_FAIL',
    payload: {
        toggle,
    }
});

export const toggleReactions = (enableReactions) => dispatch => {
    dispatch(toggleReactionsStart(enableReactions));
    return axios.put('/api/reactions', { enableReactions }).then(() => {
        dispatch(toggleReactionsSuccess());
        return true;
    }).catch(() => {
        dispatch(toggleReactionsFail(!enableReactions));
        ToastsStore.error('There was an error toggling your reactions. Please try again', 5000);
        return false;
    });
};
