const initialState = {
    isFetchingEquipment: false,
    isSavingNewCategory: false,
    isSavingCategory: null,
    isSavingNewEquipmentIn: null,
    isSavingEquipment: null,
    isDeleting: false,
    enableReactions: false,
    equipment: [],
    errors: [],
    toDelete: {
        showModal: false,
        type: '',
        id: '',
        name: '',
        parentId: '',
    }
};

const EquipmentReducer = (state = initialState, action) => {
    switch(action.type){
        case 'EQUIPMENT_FETCH_START':
            return {
                ...state,
                isFetchingEquipment: true,
                toDelete: {
                    type: '',
                    id: '',
                    name: ''
                }
            };
        case 'EQUIPMENT_FETCH_SUCCESS':
            return {
                ...state,
                isFetchingEquipment: false,
                enableReactions: action.payload.enableReactions,
                equipment: action.payload.equipment,
            };
        case 'EQUIPMENT_FETCH_FAIL':
            return {
                ...state,
                isFetchingEquipment: false,
                errors: action.payload.errors,
            };
        case 'EQUIPMENT_CATEGORY_ADD_START':
            return {
                ...state,
                isSavingNewCategory: true,
            };
        case 'EQUIPMENT_CATEGORY_ADD_SUCCESS':
            return {
                ...state,
                isSavingNewCategory: false,
                equipment: [
                    ...state.equipment,
                    action.payload.category
                ],
            };
        case 'EQUIPMENT_CATEGORY_ADD_FAIL':
            return {
                ...state,
                isSavingNewCategory: false,
                errors: action.payload.errors,
            };
        case 'EQUIPMENT_CATEGORY_EDIT_START':
            return {
                ...state,
                isSavingCategory: action.payload.id,
            };
        case 'EQUIPMENT_CATEGORY_EDIT_SUCCESS':
            return {
                ...state,
                isSavingCategory: null,
                equipment: state.equipment.map(c => (
                    c.uuid === action.payload.id ? { ...c, name: action.payload.name } : c
                )),
            };
        case 'EQUIPMENT_CATEGORY_EDIT_FAIL':
            return {
                ...state,
                isSavingCategory: false,
                errors: action.payload.errors,
            };
        case 'EQUIPMENT_DELETE_MODAL_SET_PARAMS':
            return {
                ...state,
                toDelete: {
                    showModal: action.payload.toggle,
                    type: action.payload.type,
                    id: action.payload.id,
                    name: action.payload.name,
                    parentId: action.payload.parentId,
                }
            };
        case 'EQUIPMENT_CATEGORY_DELETE_START':
            return {
                ...state,
                isDeleting: true,
            };
        case 'EQUIPMENT_CATEGORY_DELETE_SUCCESS':
            return {
                ...state,
                equipment: state.equipment.filter(c => c.uuid !== action.payload.id),
                isDeleting: false,
            };
        case 'EQUIPMENT_CATEGORY_DELETE_FAIL':
            return {
                ...state,
                isDeleting: false,
            };
        case 'EQUIPMENT_ADD_START':
            return {
                ...state,
                isSavingNewEquipmentIn: action.payload.categoryId,
            };
        case 'EQUIPMENT_ADD_SUCCESS':
            return {
                ...state,
                isSavingNewEquipmentIn: null,
                equipment: state.equipment.map(c => (
                    c.uuid === action.payload.categoryId
                        ? { ...c, equipment: [...c.equipment, action.payload.equipment] }
                        : c
                ))
            };
        case 'EQUIPMENT_ADD_FAIL':
            return {
                ...state,
                isSavingNewEquipmentIn: null,
            };
        case 'EQUIPMENT_EDIT_START':
            return {
                ...state,
                isSavingEquipment: action.payload.id,
            };
        case 'EQUIPMENT_EDIT_SUCCESS':
            return {
                ...state,
                isSavingEquipment: null,
                equipment: state.equipment.map(c => (
                    c.uuid === action.payload.categoryId
                        ? {
                            ...c,
                            equipment: c.equipment.map(e => (
                                e.uuid === action.payload.equipment.id
                                    ? {
                                        ...e,
                                        name: action.payload.equipment.name,
                                        primary_category: action.payload.equipment.primaryCategory,
                                        secondary_category: action.payload.equipment.secondaryCategory,
                                    }
                                    : e
                            ))
                        }
                        : c
                ))
            };
        case 'EQUIPMENT_EDIT_FAIL':
            return {
                ...state,
                isSavingEquipment: null,
            };
        case 'EQUIPMENT_DELETE_START':
            return {
                ...state,
                isDeleting: true,
            };
        case 'EQUIPMENT_DELETE_SUCCESS':
            return {
                ...state,
                equipment: state.equipment.map(c => (
                    c.uuid === action.payload.categoryId
                        ? {
                            ...c,
                            equipment: c.equipment.filter(e => e.uuid !== action.payload.id)
                        }
                        : c
                )),
                isDeleting: false,
            };
        case 'EQUIPMENT_DELETE_FAIL':
            return {
                ...state,
                isDeleting: false,
            };
        case 'EQUIPMENT_TOGGLE_REACTIONS_START': {
            return {
                ...state,
                enableReactions: action.payload.toggle,
            }
        }
        case 'EQUIPMENT_TOGGLE_REACTIONS_FAIL': {
            return {
                ...state,
                enableReactions: action.payload.toggle,
            }
        }
        default:
            return state;
    }
};

export default EquipmentReducer;
