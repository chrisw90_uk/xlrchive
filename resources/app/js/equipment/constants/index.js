export const CATEGORIES = {
    instruments: {
        title: 'Instruments',
        subCategories: {
            percussion: 'Percussion',
            guitar: 'Guitar',
            bass: 'Bass Guitar',
            brass: 'Brass',
            strings: 'Strings',
            woodwind: 'Woodwind',
            other: 'Other'
        }
    },
    audio: {
        title: 'Audio',
        subCategories: {
            interfaces: 'Interfaces',
            controllers: 'Controllers',
            mixers: 'Mixers',
            monitors: 'Monitors',
            speakers: 'Speakers',
            microphones: 'Microphones',
            furniture: 'Furniture',
            headphones: 'Headphones',
            software: 'Software',
            synthesisers: 'Synthesisers',
            samplers: 'Samplers',
            accessories: 'Accessories',
            outboard: 'Outboard',
            recorders: 'Recorders',
            pa: 'PA Systems',
            other: 'Other'
        }
    },
    lighting: {
        title: 'Lighting',
        subCategories: {
            stage_lighting: 'Stage Lighting',
            controllers: 'Controllers',
            effects: 'Effects',
            accessories: 'Accessories',
            other: 'Other'
        }
    },
    dj: {
        title: 'DJ',
        subCategories: {
            decks: 'Decks',
            mixers: 'Mixers',
            controllers: 'Controllers',
            headphones: 'Headphones',
            scratch: 'Scratch',
            accessories: 'Accessories',
            effects: 'Effects',
            other: 'Other'
        }
    }
};
