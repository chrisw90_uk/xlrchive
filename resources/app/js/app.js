import React from 'react';
import ReactDOM from 'react-dom';
import App from "./router/components/Router";
import './../css/app.scss';

ReactDOM.render(
    <App />,
    document.getElementById('app')
);
