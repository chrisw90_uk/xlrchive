import React from 'react';
import { connect } from 'react-redux'
import { fetchAccount } from "../actions";
import Account from '../components/Account';

const mapStateToProps = state => ({
    isFetching: state.account.isFetching,
});

const mapDispatchToProps = (dispatch) => ({
    fetchAccount: () => dispatch(fetchAccount()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Account);
