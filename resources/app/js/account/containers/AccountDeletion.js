import React from 'react';
import { connect } from 'react-redux';
import {
    deactivateAccount,
    deleteAccount,
} from "../actions";
import AccountDeletion from '../components/AccountDeletion';

const mapStateToProps = state => ({
    isDeactivatingAccount: state.account.isDeactivatingAccount,
    isDeletingAccount: state.account.isDeletingAccount,
});

const mapDispatchToProps = (dispatch) => ({
    deactivateAccount: (password, confirmPassword) => dispatch(deactivateAccount(password, confirmPassword)),
    deleteAccount: (password, confirmPassword) => dispatch(deleteAccount(password, confirmPassword)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AccountDeletion);
