import React from 'react';
import { connect } from 'react-redux'
import { saveAccountChanges } from "../actions";
import AccountDetails from '../components/AccountDetails';

const mapStateToProps = state => ({
    isSaving: state.account.isSavingAccountDetails,
    name: state.account.user.name,
    username: state.account.user.username,
    email: state.account.user.email,
    emailNotifications: state.account.user.emailNotifications,
    excludeFromSearch: state.account.user.excludeFromSearch
});

const mapDispatchToProps = (dispatch) => ({
    saveAccountChanges: user => dispatch(saveAccountChanges(user))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AccountDetails);
