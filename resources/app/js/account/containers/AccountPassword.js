import React from 'react';
import { connect } from 'react-redux'
import { savePassword } from "../actions";
import AccountPassword from '../components/AccountPassword';

const mapStateToProps = state => ({
    isSaving: state.account.isSavingNewPassword,
});

const mapDispatchToProps = (dispatch) => ({
    savePassword: (currentPassword, password, confirmPassword) => dispatch(
        savePassword(currentPassword, password, confirmPassword)
    )
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AccountPassword);
