export const PASSWORD_CRITERIA = {
    capital: {
        label: 'At least one capital letter',
        test: /[A-Z]/
    },
    number: {
        label: 'At least one number',
        test: /[0-9]/
    }
};
