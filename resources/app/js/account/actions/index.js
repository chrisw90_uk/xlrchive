import axios from 'axios';
import { ToastsStore } from "react-toasts";
import moment from 'moment';

// Fetch account

const fetchAccountStart = () => ({
    type: 'ACCOUNT_FETCH_START',
});

const fetchAccountSuccess = (
    name, username, email, emailNotifications, excludeFromSearch, unreadNotificationCount, notifications
) => ({
    type: 'ACCOUNT_FETCH_SUCCESS',
    payload: {
        name,
        username,
        email,
        emailNotifications,
        excludeFromSearch,
        unreadNotificationCount,
        notifications,
    }
});

const fetchAccountFail = errors => ({
    type: 'ACCOUNT_FETCH_FAIL',
    payload: {
        errors,
    }
});

export const fetchAccount = () => dispatch => {
    dispatch(fetchAccountStart());
    axios.get('/api/user/account').then(res => {
        const {
            name,
            username,
            email,
            emailNotifications,
            excludeFromSearch,
            notifications,
            unreadNotificationCount,
        } = res.data;
        dispatch(fetchAccountSuccess(name, username, email, emailNotifications, excludeFromSearch, unreadNotificationCount, notifications));
        setTimeout(() => dispatch(fetchNotifications()), 30000);
    }).catch(err => {
        dispatch(fetchAccountFail(err.response.data));
        ToastsStore.error(err.response.data, 5000);
    });
};

// Save account changes

const saveAccountChangesStart = () => ({
    type: 'ACCOUNT_SAVE_START',
});

const saveAccountChangesSuccess = (name, username, email) => ({
    type: 'ACCOUNT_SAVE_SUCCESS',
    payload: {
        name,
        username,
        email,
    }
});

const saveAccountChangesFail = errors => ({
    type: 'ACCOUNT_SAVE_FAIL',
    payload: {
        errors,
    }
});

export const saveAccountChanges = user => dispatch => {
    dispatch(saveAccountChangesStart());
    axios.put('/api/user/account', user).then(() => {
        dispatch(saveAccountChangesSuccess(user.name, user.username, user.email));
        ToastsStore.success('Details saved successfully', 5000);
    }).catch(err => {
        const { errors } = err.response.data;
        dispatch(saveAccountChangesFail(errors));
        ToastsStore.error(errors.join('\n'), 5000);
    });
};

// Save password

const savePasswordStart = () => ({
    type: 'ACCOUNT_PASSWORD_SAVE_START',
});

const savePasswordSuccess = () => ({
    type: 'ACCOUNT_PASSWORD_SAVE_SUCCESS',
});

const savePasswordFail = errors => ({
    type: 'ACCOUNT_PASSWORD_SAVE_FAIL',
    payload: {
        errors,
    }
});

export const savePassword = (currentPassword, password, password_confirmation) => dispatch => {
    dispatch(savePasswordStart());
    return axios.put(
        '/api/user/password',
        { currentPassword, password, password_confirmation }
    ).then(() => {
        dispatch(savePasswordSuccess());
        ToastsStore.success('Your password has been changed', 5000);
        return true;
    }).catch(err => {
        const { errors } = err.response.data;
        dispatch(savePasswordFail(errors));
        ToastsStore.error(errors.join('\n'), 5000);
        return false;
    });
};

// Deactivate account

const deactivateAccountStart = () => ({
    type: 'ACCOUNT_DEACTIVATE_START',
});

const deactivateAccountSuccess = () => ({
    type: 'ACCOUNT_DEACTIVATE_SUCCESS',
});

const deactivateAccountFail = errors => ({
    type: 'ACCOUNT_DEACTIVATE_FAIL',
    payload: {
        errors,
    }
});

export const deactivateAccount = (password, password_confirmation) => dispatch => {
    dispatch(deactivateAccountStart());
    return axios.put(
        '/api/user/deactivate',
        { password, password_confirmation }
    ).then(() => {
        dispatch(deactivateAccountSuccess());
        return window.location.replace('/');
    }).catch(err => {
        const { errors } = err.response.data;
        dispatch(deactivateAccountFail(errors));
        ToastsStore.error(errors.join('\n'), 5000);
        return false;
    });
};

// Deactivate account

const deleteAccountStart = () => ({
    type: 'ACCOUNT_DELETE_START',
});

const deleteAccountSuccess = () => ({
    type: 'ACCOUNT_DELETE_SUCCESS',
});

const deleteAccountFail = errors => ({
    type: 'ACCOUNT_DELETE_FAIL',
    payload: {
        errors,
    }
});

export const deleteAccount = (password, password_confirmation) => dispatch => {
    dispatch(deleteAccountStart());
    return axios.put(
        '/api/user/delete',
        { password, password_confirmation }
    ).then(() => {
        dispatch(deleteAccountSuccess());
        return window.location.replace('/');
    }).catch(err => {
        const { errors } = err.response.data;
        dispatch(deleteAccountFail(errors));
        ToastsStore.error(errors.join('\n'), 5000);
        return false;
    });
};

// Notifications

const fetchNotificationsStart = () => ({
    type: 'NOTIFICATIONS_FETCH_START'
});

const fetchNotificationsSuccess = (unreadNotificationCount, notifications) => ({
    type: 'NOTIFICATIONS_FETCH_SUCCESS',
    payload: {
        unreadNotificationCount,
        notifications,
    }
});

const fetchNotificationsFail = (err) => ({
    type: 'NOTIFICATIONS_FETCH_FAIL',
    payload: {
        err
    }
});

const fetchNotifications = () => (dispatch, getState) => {
    dispatch(fetchNotificationsStart());
    axios.get(
        '/api/notifications',
    ).then(async res => {
        const { unreadNotificationCount, notifications: newNotifications } = res.data;
        const { flatNotifications: currentNotifications } = getState().account;
        const notifications = [
            ...newNotifications,
            ...currentNotifications.filter(n => !newNotifications.some(ntn => ntn.id === n.id)),

        ];
        dispatch(fetchNotificationsSuccess(
            unreadNotificationCount,
            notifications,
        ));
        setTimeout(() => dispatch(fetchNotifications()), 30000);
    }).catch((err) => {
        dispatch(fetchNotificationsFail(err));
        setTimeout(() => dispatch(fetchNotifications()), 30000);
    });
};

const deleteNotificationStart = (id, decrement) => ({
    type: 'NOTIFICATION_DELETE_START',
    payload: {
        id,
        decrement
    }
});

const deleteNotificationSuccess = () => ({
    type: 'NOTIFICATION_DELETE_SUCCESS',
});

const deleteNotificationFail = (notifications, decrement) => ({
    type: 'NOTIFICATION_DELETE_FAIL',
    payload: {
        notifications,
        decrement
    }
});

export const deleteNotification = (id, group, seen) => (dispatch, getState) => {
    const { flatNotifications } = getState().account;
    const decrement = seen ? 0 : 1;
    dispatch(deleteNotificationStart(id, decrement));
    return axios.post(
        '/api/notification/delete',
        { id }
    ).then(() => {
        dispatch(deleteNotificationSuccess());
    }).catch(err => {
        const { errors } = err.response.data;
        dispatch(deleteNotificationFail(flatNotifications, -decrement));
        return ToastsStore.error(errors.join('\n'), 5000);
    });
};

const updateNotificationsToReadStart = id => ({
    type: 'NOTIFICATION_MARK_READ_START',
    payload: {
        id
    }
});

const updateNotificationsToReadSuccess = () => ({
    type: 'NOTIFICATION_MARK_READ_SUCCESS',
});

const updateNotificationsToReadFail = () => ({
    type: 'NOTIFICATION_MARK_READ_FAIL',
});

export const markNotificationsAsRead = ids => dispatch => {
    dispatch(updateNotificationsToRead(ids));
    dispatch(updateNotificationsToReadStart());
    return axios.put(
        '/api/notifications',
        { ids }
    ).then(() => {
        dispatch(updateNotificationsToReadSuccess());
    }).catch(err => {
        const { errors } = err.response.data;
        dispatch(updateNotificationsToReadFail());
        return ToastsStore.error(errors.join('\n'), 5000);
    });
};

const updateNotificationsToRead = ids => ({
    type: 'NOTIFICATIONS_MARK_READ',
    payload: {
        ids,
    }
});
