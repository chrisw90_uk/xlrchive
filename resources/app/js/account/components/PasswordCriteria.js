import React from 'react';

const PasswordCriteria = ({
    met,
    label,
}) => (
    <li className={`details__password-criteria ${met ? 'details__password-criteria--met' : ''}`.trim()}>
        <i className="material-icons">{met ? 'check_circle' : 'remove_circle'}</i>
        <span>{label}</span>
    </li>
);

export default PasswordCriteria;
