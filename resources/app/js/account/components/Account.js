import React from 'react';
import AccountDetails from "../containers/AccountDetails";
import AccountPassword from "../containers/AccountPassword";
import AccountDeletion from "../containers/AccountDeletion";

const Account = ({
    isFetching,
}) => (
    <div className="details full bg--red pad--t">
        <div className="text--center">
            <h1>Account.</h1>
            {isFetching ? (
                <div>
                    <p>Loading...</p>
                </div>
            ) : (
                <div>
                    <div className="pad--x">
                        <AccountDetails />
                        <AccountPassword />
                    </div>
                    <AccountDeletion />
                </div>
            )}
        </div>
    </div>
);

export default Account;
