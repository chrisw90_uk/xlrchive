import React from 'react';
import Button from "../../components/Button";
import { PASSWORD_CRITERIA } from "../constants";
import PasswordCriteria from "./PasswordCriteria";

class AccountPassword extends React.Component {

    state = {
        currentPassword: '',
        password: '',
        confirmPassword: '',
    };

    isValid = () => {
        const { password, confirmPassword } = this.state;
        return password === confirmPassword
            && password.length >= 8
            && Object.keys(PASSWORD_CRITERIA).every(c => PASSWORD_CRITERIA[c].test.test(password));
    };

    updateField = (field, value) => {
        this.setState({ [field]: value })
    };

    resetFields = () => {
        this.setState({
            currentPassword: '',
            password: '',
            confirmPassword: '',
        })
    };

    render() {
        const { isSaving, savePassword } = this.props;
        const { currentPassword, password, confirmPassword } = this.state;
        return (
            <form
                className="form form--small margin--center"
            >
                <fieldset>
                    <legend className="text--center">Change password.</legend>
                    <div className="form__field form__field--white">
                        <input
                            required
                            name="currentPassword"
                            type="password"
                            value={currentPassword}
                            onChange={e => this.updateField('currentPassword', e.target.value)}
                            placeholder="Current password."
                        />
                        <label htmlFor="password">
                            Enter your current password, so we know it's you.
                        </label>
                    </div>
                    <div className="form__field form__field--white">
                        <input
                            required
                            name="password"
                            type="password"
                            value={password}
                            onChange={e => this.updateField('password', e.target.value)}
                            placeholder="Password."
                        />
                        <label htmlFor="password">
                            Specify a strong new password.
                        </label>
                        <div>
                            <ul className="margin--t-md">
                                <PasswordCriteria
                                    met={password.length >= 8}
                                    label="At least 8 characters"
                                />
                                {Object.keys(PASSWORD_CRITERIA).map(c => (
                                    <PasswordCriteria
                                        key={c}
                                        met={PASSWORD_CRITERIA[c].test.test(password)}
                                        label={PASSWORD_CRITERIA[c].label}
                                    />
                                ))}
                            </ul>
                        </div>
                    </div>
                    <div className="form__field form__field--white">
                        <input
                            required
                            name="confirmPassword"
                            type="password"
                            value={confirmPassword}
                            onChange={e => this.updateField('confirmPassword', e.target.value)}
                            placeholder="Confirm new password."
                        />
                        <label htmlFor="confirmPassword">
                            Type the same thing again.
                        </label>
                    </div>
                </fieldset>
                <div className="text--right">
                    <Button
                        className="btn--outline btn--hover-white"
                        type="submit"
                        onClick={async e => {
                            e.preventDefault();
                            const success = await savePassword(currentPassword, password, confirmPassword);
                            if (success) this.resetFields();
                        }}
                        disabled={!this.isValid() || isSaving}
                        working={isSaving}
                        label="Save"
                    />
                </div>
            </form>
        )
    }

}

export default AccountPassword;
