import React from 'react';
import Button from "../../components/Button";
import Modal from "../../components/Modal";

class AccountDeletion extends React.Component {
    state = {
        password: '',
        confirmPassword: '',
        showWarningModal: false,
    };
    updateField = (field, value) => {
        this.setState({ [field]: value });
    };
    isValid = () => {
        const {
            password, confirmPassword,
        } = this.state;
        return password.length > 0 && password === confirmPassword;
    };
    render() {
        const {
            isDeactivatingAccount,
            isDeletingAccount,
            deactivateAccount,
            deleteAccount,
        } = this.props;
        const { showWarningModal, password, confirmPassword } = this.state;
        return (
            <div className="bg--off-black pad--x pad--y margin--t-xl">
                {showWarningModal && (
                    <Modal
                        title="Are you sure?"
                        onCancel={() => this.setState({ showWarningModal: false })}
                        cancelAction="Cancel"
                        onConfirm={() => deleteAccount(password, confirmPassword)}
                        confirmAction="Delete Account"
                        working={isDeletingAccount}
                    >
                        <p>Your account will be permanently deleted. This action cannot be undone.</p>
                    </Modal>
                )}
                <form
                    className="form form--small margin--center"
                >
                    <fieldset>
                        <legend className="text--center">Close your account.</legend>
                        <div className="text--left">
                            <p>
                                We'll be sad to see you go, that's for sure; you're part of a rich community of creatives!
                            </p>
                            <p>
                                If you definitely want to leave, then simply confirm your password and your account will be
                                {' '}
                                gone forever. However, if you simply want to take a break, you can deactivate your account
                                {' '} instead. Simply log back in again to reactivate it.
                            </p>
                        </div>
                        <div className="form__field form__field--white">
                            <input
                                required
                                name="password"
                                type="password"
                                value={password}
                                onChange={e => this.updateField('password', e.target.value)}
                                placeholder="Password."
                            />
                        </div>
                        <div className="form__field form__field--white">
                            <input
                                required
                                name="confirmPassword"
                                type="password"
                                value={confirmPassword}
                                onChange={e => this.updateField('confirmPassword', e.target.value)}
                                placeholder="Confirm password."
                            />
                        </div>
                    </fieldset>
                    <div className="text--right">
                        <Button
                            className="btn--outline btn--hover-red"
                            type="button"
                            onClick={() => deactivateAccount(password, confirmPassword)}
                            label="Deactivate"
                            disabled={!this.isValid() || isDeactivatingAccount || isDeletingAccount}
                            working={isDeactivatingAccount}
                        />
                        <Button
                            className="btn--outline btn--hover-red"
                            type="button"
                            onClick={() => this.setState({ showWarningModal: true })}
                            label="Delete"
                            disabled={!this.isValid() || isDeletingAccount || isDeactivatingAccount}
                            working={isDeletingAccount}
                        />
                    </div>
                </form>
            </div>
        )
    }
}

export default AccountDeletion;
