import React from 'react';
import Button from "../../components/Button";

class AccountDetails extends React.Component {

    state = {
        name: '',
        username: '',
        email: '',
        emailNotifications: true,
        excludeFromSearch: false,
    };

    componentDidMount() {
        const { name, username, email, emailNotifications, excludeFromSearch } = this.props;
        console.log(this.props)
        this.setState({
            name,
            username,
            email,
            emailNotifications: emailNotifications === null ? true : emailNotifications,
            excludeFromSearch: excludeFromSearch || false,
        });
    }

    hasChanged = () => {
        return ['name', 'username', 'email', 'emailNotifications', 'excludeFromSearch'].some(field => this.state[field] !== this.props[field]);
    };

    isValid = () => {
        const { name, username, email } = this.state;
        return name.length > 0 && email.length > 0 && /^[a-zA-Z0-9-_]+$/.test(username);
    };

    updateField = (field, value) => {
        this.setState({ [field]: value })
    };

    render() {
        const { isSaving, saveAccountChanges } = this.props;
        const { name, username, email, emailNotifications, excludeFromSearch } = this.state;
        return (
            <form
                className="form form--small margin--center"
            >
                <fieldset>
                    <legend className="text--center">Basic details.</legend>
                    <div className="form__field form__field--white">
                        <input
                            required
                            name="name"
                            type="text"
                            value={name}
                            onChange={e => this.updateField('name', e.target.value)}
                            placeholder="Name."
                        />
                        <label htmlFor="name">
                            Your name. 'Nuff said.
                        </label>
                    </div>
                    <div className="form__field form__field--white">
                        <input
                            required
                            name="username"
                            type="text"
                            value={username}
                            onChange={e => this.updateField('username', e.target.value)}
                            placeholder="Username."
                        />
                        <label htmlFor="username">
                            Your unique username which doubles as your profile URL.
                            <br />Only letters, numbers, dashes and underscores please.
                        </label>
                    </div>
                    <div className="form__field form__field--white">
                        <input
                            required
                            name="email"
                            type="text"
                            value={email}
                            onChange={e => this.updateField('email', e.target.value)}
                            placeholder="Email."
                        />
                        <label htmlFor="username">
                            Your email address. We will use this to spam you aggressively.
                        </label>
                    </div>
                    <div className="form__field form__field--radio">
                        <input
                            id="emailNotifications"
                            name="emailNotifications"
                            type="checkbox"
                            onChange={() => this.updateField('emailNotifications', !emailNotifications)}
                            checked={emailNotifications}
                        />
                        <label htmlFor="emailNotifications">
                            Receive email notifications
                        </label>
                    </div>
                    <div className="form__field form__field--radio">
                        <input
                            id="excludeFromSearch"
                            name="excludeFromSearch"
                            type="checkbox"
                            onChange={() => this.updateField('excludeFromSearch', !excludeFromSearch)}
                            checked={excludeFromSearch || false}
                        />
                        <label htmlFor="excludeFromSearch">
                            Hide profile from search.
                        </label>
                    </div>
                </fieldset>
                <div className="text--right">
                    <Button
                        className="btn--outline btn--hover-white"
                        type="submit"
                        onClick={e => {
                            e.preventDefault();
                            saveAccountChanges({ name, username, email, emailNotifications, excludeFromSearch });
                        }}
                        disabled={!this.hasChanged() || !this.isValid() || isSaving}
                        working={isSaving}
                        label="Save"
                    />
                </div>
            </form>
        )
    }
}

export default AccountDetails;
