const initialState = {
    isFetching: false,
    isFetchingNotifications: false,
    isSavingAccountDetails: false,
    isSavingNewPassword: false,
    isDeactivatingAccount: false,
    isDeletingAccount: false,
    unreadNotificationCount: 0,
    user: {
        name: '',
        username: '',
        email: '',
        excludeFromSearch: false,
    },
    flatNotifications: [],
};

const AccountReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ACCOUNT_FETCH_START':
            return {
                ...state,
                isFetching: true,
                isFetchingNotifications: true,
            };
        case 'ACCOUNT_FETCH_SUCCESS':
            return {
                ...state,
                isFetching: false,
                isFetchingNotifications: false,
                flatNotifications: action.payload.notifications,
                unreadNotificationCount: action.payload.unreadNotificationCount,
                user: {
                    name: action.payload.name,
                    username: action.payload.username,
                    email: action.payload.email,
                    emailNotifications: action.payload.emailNotifications,
                    excludeFromSearch: action.payload.excludeFromSearch,
                }
            };
        case 'ACCOUNT_FETCH_FAIL':
            return {
                ...state,
                isFetching: false,
                isFetchingNotifications: false,
            };
        case 'ACCOUNT_SAVE_START':
            return {
                ...state,
                isSavingAccountDetails: true,
            };
        case 'ACCOUNT_SAVE_SUCCESS':
            return {
                ...state,
                isSavingAccountDetails: false,
                user: {
                    name: action.payload.name,
                    username: action.payload.username,
                    email: action.payload.email,
                }
            };
        case 'ACCOUNT_SAVE_FAIL':
            return {
                ...state,
                isSavingAccountDetails: false,
            };
        case 'ACCOUNT_PASSWORD_SAVE_START':
            return {
                ...state,
                isSavingNewPassword: true,
            };
        case 'ACCOUNT_PASSWORD_SAVE_SUCCESS':
            return {
                ...state,
                isSavingNewPassword: false,
            };
        case 'ACCOUNT_PASSWORD_SAVE_FAIL':
            return {
                ...state,
                isSavingNewPassword: false,
            };
        case 'ACCOUNT_DEACTIVATE_START':
            return {
                ...state,
                isDeactivatingAccount: true,
            };
        case 'ACCOUNT_DEACTIVATE_SUCCESS':
            return {
                ...state,
                isDeactivatingAccount: false,
            };
        case 'ACCOUNT_DEACTIVATE_FAIL':
            return {
                ...state,
                isDeactivatingAccount: false,
            };
        case 'ACCOUNT_DELETE_START':
            return {
                ...state,
                isDeletingAccount: true,
            };
        case 'ACCOUNT_DELETE_SUCCESS':
            return {
                ...state,
                isDeletingAccount: false,
            };
        case 'ACCOUNT_DELETE_FAIL':
            return {
                ...state,
                isDeletingAccount: false,
            };
        case 'NOTIFICATION_DELETE_START':
            return {
                ...state,
                flatNotifications: state.flatNotifications.filter(n => n.id !== action.payload.id),
                unreadNotificationCount: state.unreadNotificationCount - action.payload.decrement
            };
        case 'NOTIFICATION_DELETE_FAIL':
            return {
                ...state,
                flatNotifications: action.payload.notifications,
                unreadNotificationCount: state.unreadNotificationCount - action.payload.decrement
            };
        case 'NOTIFICATIONS_MARK_READ':
            return {
                ...state,
                unreadNotificationCount: state.unreadNotificationCount - action.payload.ids.length,
                flatNotifications: state.flatNotifications.map(n => {
                    if (action.payload.ids.includes(n.id)) {
                        return { ...n, seen: true }
                    }
                    return n;
                }),
            };
        case 'NOTIFICATIONS_FETCH_SUCCESS':
            return {
                ...state,
                unreadNotificationCount: action.payload.unreadNotificationCount,
                flatNotifications: action.payload.notifications,
            };
        default:
            return state;
    }
};

export default AccountReducer;
