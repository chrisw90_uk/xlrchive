import React from 'react';
import Spinner from '../../img/working.gif';

const Button = ({
    disabled,
    type,
    className,
    onClick,
    label,
    working,
}) => (
    <button
        disabled={disabled}
        className={`btn ${working ? 'btn--loading' : ''} ${className}`.trim()}
        type={type || 'button'}
        onClick={onClick}
    >
        {working && (
            <img alt="Working..." src={Spinner} />
        )}
        <span>
            {label}
        </span>
    </button>
);

export default Button;
