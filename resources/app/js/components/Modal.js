import React from 'react';
import Button from "./Button";

const Modal = ({
    title,
    children,
    onCancel,
    cancelAction,
    onConfirm,
    confirmAction,
    disableOnConfirm,
    working,
    disableControls,
    hideConfirmAction,
}) => (
    <div className="modal">
        <div className="modal__body pad--x pad--y">
            <h3>{title}</h3>
            <div className="modal__content">
                {children}
            </div>
            {!disableControls && (
                <div className="text--right">
                    <Button
                        disabled={working}
                        className="btn--outline btn--hover-red"
                        onClick={onCancel}
                        label={cancelAction}
                    />
                    {!hideConfirmAction && (
                        <Button
                            disabled={working || disableOnConfirm}
                            className="btn--outline btn--hover-red"
                            onClick={onConfirm}
                            label={confirmAction}
                            working={working}
                        />
                    )}
                </div>
            )}
        </div>
    </div>
);

export default Modal;
