import React from 'react';
import Dropzone from "react-dropzone";

const ImageUploadWithoutCrop = ({
    onImageUpload,
    children,
}) => (
    <Dropzone onDrop={acceptedFiles => onImageUpload(acceptedFiles)}>
        {({getRootProps, getInputProps}) => (
            <div className="image-upload__input" {...getRootProps()}>
                <input {...getInputProps()} multiple={false}/>
                <label>Drag and drop your image or click to select an image.</label>
                {children}
            </div>
        )}
    </Dropzone>
);

export default ImageUploadWithoutCrop;
