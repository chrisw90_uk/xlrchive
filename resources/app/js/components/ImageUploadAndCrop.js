import React from 'react';
import ReactCrop from "react-image-crop";
import { IMAGE_TYPES } from "../constants";
import Dropzone from "react-dropzone";

const ImageUploadAndCrop = ({
    minWidth,
    croppingImage,
    uploadedImageAsBase64,
    crop,
    onImageUpload,
    onChangeCrop,
    children,
}) => (
    <React.Fragment>
        {croppingImage ? (
            <div className="image-upload__edit">
                <ReactCrop
                    minWidth={minWidth}
                    src={uploadedImageAsBase64}
                    crop={crop}
                    onChange={crop => onChangeCrop(crop)}
                />
            </div>
        ) : (
            <Dropzone accept={IMAGE_TYPES} onDrop={acceptedFiles => onImageUpload(acceptedFiles)}>
                {({getRootProps, getInputProps}) => (
                    <div className="image-upload__input" {...getRootProps()}>
                        <input {...getInputProps()} multiple={false}/>
                        <label>Drag and drop your image or click to select an image.</label>
                        {children}
                    </div>
                )}
            </Dropzone>
        )}
    </React.Fragment>
);

export default ImageUploadAndCrop;
