import React from 'react';

const ImageUploadButton = ({
    onClick,
    icon,
    label,
}) => (
    <button
        type="button"
        onClick={onClick}
        className="image-upload"
    >
        {icon}
        <span>{label}</span>
    </button>
);

export default ImageUploadButton;
