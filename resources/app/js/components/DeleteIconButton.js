import React from 'react';

const DeleteIconButton = ({
    className,
    onClick,
}) => (
    <button
        className={`btn btn--icon btn--red ${className}`.trim()}
        type="button"
        onClick={onClick}
    >
        <i className="material-icons">
            clear
        </i>
    </button>
);

export default DeleteIconButton;
