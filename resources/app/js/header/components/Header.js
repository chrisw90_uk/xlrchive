import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom';

const Header = ({
    user,
    fetchAccount,
    unreadNotificationCount,
}) => {
    useEffect(() => {
        fetchAccount();
    }, []);
    return (
        <header className="header">
            <div className="flex align--center justify--between wrapper header__inner">
                <div className="flex align--center">
                    <span className="header__logo">XLRchive.</span>
                    <nav className="header__site-nav">
                        <a href="/search">
                            <i className="material-icons">
                                person_search
                            </i>
                            <span className="header__site-nav-label">
                                Search
                            </span>
                        </a>
                        <NavLink to="/dashboard/notifications">
                            <i className="material-icons">
                                notifications
                            </i>
                            <span className="header__site-nav-label">
                                Notifications
                            </span>
                            {unreadNotificationCount > 0 && (
                                <span className="header__alert">
                                    {unreadNotificationCount > 9 ? '9+' : unreadNotificationCount}
                                </span>
                            )}
                        </NavLink>
                    </nav>
                </div>
                <nav className="header__nav">
                    <NavLink activeClassName="header__nav--active-red" to="/dashboard/details">Details</NavLink>
                    <NavLink activeClassName="header__nav--active-black" to="/dashboard/equipment">Equipment</NavLink>
                    <NavLink activeClassName="header__nav--active-red" to="/dashboard/account">Account</NavLink>
                </nav>
                <div>
                    {user.username && (
                        <a className="btn btn--outline btn--hover-red" href={`/user/${user.username}`}>
                            View profile
                        </a>
                    )}
                    <button
                        onClick={() => window.location.replace('/logout')}
                        className="btn btn--outline btn--hover-red"
                    >
                        Logout
                    </button>
                </div>
            </div>
        </header>
    )
};

export default Header;
