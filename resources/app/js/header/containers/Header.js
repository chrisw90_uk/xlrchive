import React from 'react';
import { connect } from 'react-redux'
import { fetchAccount } from "../../account/actions";
import Header from '../components/Header';

const mapStateToProps = state => ({
    user: state.account.user,
    unreadNotificationCount: state.account.unreadNotificationCount,
});

const mapDispatchToProps = (dispatch) => ({
    fetchAccount: () => dispatch(fetchAccount()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);
