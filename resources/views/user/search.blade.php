@extends('layouts.head')

@section('content')
    @include('layouts.header')
    <div class="search full--w-footer bg--off-black">
        <div class="wrapper pad--x">
            <section class="page__header">
                <h1>User search.</h1>
            </section>
            <section class="wrapper wrapper--small text--center margin--b-md">
                <p>
                    Search our database of wonderful users with the option to filter by display name or user
                    category. Simply click on a user to visit their profile and check out their gear.
                </p>
            </section>
            <section class="search__filter">
                <form method="GET" action="{{ route('search') }}">
                    <div class="search__filter-list">
                        <div class="form__field">
                            <input
                                id="displayName"
                                type="text"
                                name="displayName"
                                value="{{Request::get('displayName')}}"
                                placeholder="Display name."
                            />
                        </div>
                        <div class="form__field form__field--select">
                            <?php $categories = Config::get('user-categories'); ?>
                            <select
                                id="category"
                                name="category"
                            >
                                <option value="" class="placeholder">All categories.</option>
                                @foreach (array_keys($categories) as $cat)
                                    <option
                                        value="{{$cat}}"
                                        {{Request::get('category') === $cat ? 'selected' : ''}}
                                    >
                                        {{$categories[$cat]}}.
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="text--right">
                        @if (Request::get('category') || Request::get('displayName'))
                            <a href="/search" class="btn btn--outline btn--hover-red">Reset</a>
                        @endif
                        <button type="submit" class="btn btn--outline btn--hover-red">Search</button>
                    </div>
                </form>
            </section>
            <section class="search__content">
                <span>
                    Showing
                    {{Request::get('category') ? $categories[Request::get('category')] : 'all'}}
                    users
                    @if (Request::get('displayName'))
                        containing '{{Request::get('displayName')}}'
                    @else
                    ({{$users->count()}})
                </span>
                @endif
                @if (count($users) === 0)
                    <p class="text--center">
                        No users matched your search. Sad times.
                    </p>
                @else
                    <ul class="search__list">
                        @foreach($users as $user)
                            <li class="search__list-item">
                                <a class="search__list-item-inner" href="/user/{{$user->username}}">
                                    <div class="search__list-item-image">
                                        @if ($user->profile_image_url)
                                            <img src="{{$user->profile_image_url}}" alt="{{$user->username}}" />
                                        @else
                                            <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-user" width="70%" height="70%"
                                                 viewBox="0 0 24 24" strokeWidth="4" stroke="white" fill="none" strokeLinecap="round"
                                                 strokeLinejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z"/>
                                                <circle cx="12" cy="7" r="4"/>
                                                <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"/>
                                            </svg>
                                        @endif
                                    </div>
                                    <div class="search__list-item-details">
                                        <h5>{{$user->display_name}}</h5>
                                        @if ($user->headline)<p class="margin--b-none">{{$user->headline}}</p>@endif
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="flex justify--between align--center">
                        <span>Page {{$users->currentPage()}} of {{ceil($user->count() / $users->perPage())}} ({{$users->firstItem()}} - {{$users->lastItem()}})</span>
                        {{$users->appends(request()->query())->links()}}
                    </div>
                @endif
            </section>
        </div>
    </div>
    @include('layouts.footer')
@endsection


