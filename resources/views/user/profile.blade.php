@extends('layouts.head')

@section('content')
    @include('layouts.header')
    <?php $ownProfile = json_encode($auth_user_id === $user['id']); ?>
    <div class="user full--w-footer bg--off-black">
        <div>
            <section class="user__header">
                <div class="user__header-col user__header-details pad--y pad--x">
                    <div class="user__image">
                        @if ($user->profile_image_url)
                            <img alt="{{$user->display_name}}" src="{{$user->profile_image_url}}" />
                        @else
                            <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-user" width="70%" height="70%"
                                 viewBox="0 0 24 24" strokeWidth="4" stroke="white" fill="none" strokeLinecap="round"
                                 strokeLinejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z"/>
                                <circle cx="12" cy="7" r="4"/>
                                <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"/>
                            </svg>
                        @endif
                    </div>
                    @if (!is_null($user->facebook) || !is_null($user->twitter) || !is_null($user->instagram) || !is_null($user->youtube))
                        <div class="user__social">
                            @if (!is_null($user->facebook))
                                <a href="https://facebook.com/{{$user->facebook}}" target="_blank" rel="noreferrer">
                                    <img alt="Facebook" src="/dev/facebook.png" />
                                </a>
                            @endif
                            @if (!is_null($user->twitter))
                                <a href="https://twitter.com/{{$user->twitter}}" target="_blank" rel="noreferrer">
                                    <img alt="Twitter" src="/dev/twitter.png" />
                                </a>
                            @endif
                            @if (!is_null($user->instagram))
                                <a href="https://instagram.com/{{$user->instagram}}" target="_blank" rel="noreferrer">
                                    <img alt="Instagram" src="/dev/instagram.png" />
                                </a>
                            @endif
                            @if (!is_null($user->youtube))
                                <a href="https://youtube.com/{{$user->youtube}}" target="_blank" rel="noreferrer">
                                    <img alt="YouTube" src="/dev/youtube.png" />
                                </a>
                            @endif
                        </div>
                    @endif
                    <div class="user__details">
                        <h1 class="user__details-name">{{$user->display_name}}</h1>
                        @if (!is_null($user->headline))
                            <h3 class="user__details-headline">{{$user->headline}}</h3>
                        @endif
                    </div>
                </div>
                @if (!is_null($user->display_image_url))
                    <div
                        class="user__header-col user__header-image"
                        style='background-image: url("{{$user->display_image_url}}")'
                    >
                        <img alt="{{$user->display_name}} header image" src="{{$user->display_image_url}}">
                    </div>
                @endif
            </section>
        </div>
        @if ($user->bio)
            <section class="user__bio bg--red pad--y">
                <div class="wrapper wrapper--small pad--x">
                    <h2 class="text--center">Bio.</h2>
                    <div class="text--left">
                        <p>
                            {!! nl2br($user->bio) !!}
                        </p>
                    </div>
                </div>
            </section>
        @endif
        <section class="user__equipment pad--y">
            <div class="wrapper pad--x">
                <h2 class="text--center">Equipment.</h2>
                @if (count($user->equipment_categories) > 0)
                    <ul class="user__equipment-categories">
                        @foreach ($user->equipment_categories as $cat)
                            @if (count($cat['equipment']) > 0)
                                <li class="user__equipment-categories-item">
                                    <h3>{{$cat['name']}}</h3>
                                    <ul class="user__equipment-list">
                                        @foreach ($cat['equipment'] as $e)
                                            <li class="user__equipment-list-item">
                                                @if (!is_null($e['image_url']))
                                                    <div class="user__equipment-image">
                                                        <img alt="{{$e['name']}}" src="{{$e['image_url']}}" />
                                                    </div>
                                                @endif
                                                <div class="user__equipment-details">
                                                    <h5>{{$e['name']}}</h5>
                                                    <span class="text--muted">
                                                        {{Config::get('equipment-item-categories.'.$e['primary_category'].'.title')}}
                                                        >
                                                        {{Config::get('equipment-item-categories.'.$e['primary_category'].'.subCategories.'.$e['secondary_category'])}}
                                                    </span>
                                                    @if ($user->enable_reactions !== 0)
                                                        <?php
                                                            $countKey = array_search($e['uuid'], array_column($reactionCounts, 'equipment_uuid'));
                                                            $reactionKey = array_search($e['uuid'], array_column($reactions, 'equipment_uuid'));
                                                            $rType = $reactionKey !== false ? $reactions[$reactionKey]['reaction_type'] : null;
                                                            $rId = $reactionKey !== false ? $reactions[$reactionKey]['uuid'] : null;
                                                            $eReactions = $countKey !== false ? json_encode($reactionCounts[$countKey]['reactions']) : "{}";
                                                        ?>
                                                        <div
                                                            class="reacts"
                                                            data-disable-reactions={{$ownProfile}}
                                                            data-equipment-id="{{$e['uuid']}}"
                                                            data-reacting-user-id="{{$auth_user_id}}"
                                                            data-current-reaction-id="{{$rId}}"
                                                            data-current-reaction-type="{{$rType}}"
                                                            data-reactions="{{$eReactions}}"
                                                        >
                                                        </div>
                                                    @endif
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                @else
                    <p class="text--center">
                        {{$user->display_name}} has not listed any equipment. Sad times.
                    </p>
                @endif
            </div>
        </section>
        <section class="qa pad--y">
            <div class="wrapper wrapper--medium pad--x">
                <h2 class="text--center margin--b-xl">Discussion.</h2>
                <div
                    id="discussion"
                    data-profile="{{$user->username}}"
                    data-author-id="{{$auth_user_id}}"
                    data-recipient-id="{{$user->id}}"
                ></div>
            </div>
        </section>
        @if ($user->enable_reactions !== 0)
            <script type="application/javascript" src="/js/reacts.bundle.js"></script>
        @endif
        <script type="application/javascript" src="/js/discussion.bundle.js"></script>
    </div>
    @include('layouts.footer')
@endsection
