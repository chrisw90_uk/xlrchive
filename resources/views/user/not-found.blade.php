@extends('layouts.head')

@section('content')
    <div class="not-found bg--off-black full flex justify--center align--center">
        <div class="not-found__content text--center">
            <h1 class="margin--b-none">404</h1>
            <h4 class="margin--b-md">
                <em>'Testing, one... two...'</em>
            </h4>
            <p>We're afraid this user doesn't exist, or they've blocked you. Savage.</p>
            <div class="margin--t-md">
                <a href="/" class="btn btn--outline btn--hover-red">
                    Go home
                </a>
            </div>
        </div>
    </div>
@endsection
