@extends('layouts.head')

@section('content')
    <div class="login">
        <section class="login__col login__col--left text--center bg--red">
            <h1>XLRchive.</h1>
            <h4>I'm a slogan that's to be confirmed.</h4>
            <nav>
                <a class="btn btn--small btn--flex btn--outline btn--hover-white" href="/search">
                    <i class="material-icons">person_search</i>
                    Search users.
                </a>
                <a class="btn btn--small btn--flex btn--outline btn--hover-white" href="/about">
                    <i class="material-icons">help</i>
                    About.
                </a>
            </nav>
            <div class="login__new page text--left">
                <h5>What's new?</h5>
                <p><strong>12th January 2021</strong> (v1.3.2) - Notification bug fixes</p>
            </div>
        </section>
        <section class="login__col login__col--right bg--off-black">
            @guest
                <form class="login__form form--small" method="POST" action="{{route('login')}}">
                    @csrf
                    <h2>Login.</h2>
                    <div class="form__field">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" required autocomplete="email" placeholder="Email">
                        @error('email')
                            <span class="form__field-error" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form__field">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password.">
                        @error('password')
                            <span class="form__field-error" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="text--right">
                        <button type="submit" class="btn btn--outline btn--hover-red">
                            Login
                        </button>
                    </div>
                    <div class="login__links text--center margin--t-sm">
                        <span>
                            <a class="margin--x-sm" href="/password/reset">I've forgotten my password</a>
                            |
                            <a class="margin--x-sm" href="/register">Sign up to XLRchive</a>
                        </span>
                    </div>
                </form>
            @else
                <h4>Hello, {{$user->username}}!</h4>
                <div class="margin--t-md flex align--center flex--wrap justify--center text--center">
                    <a class="btn btn--outline btn--hover-white btn--flex" href="/dashboard">
                        <i class="material-icons">dashboard</i>
                        Dashboard
                    </a>
                    <a class="btn btn--outline btn--hover-white btn--flex" href="/user/{{$user->username}}">
                        <i class="material-icons">person</i>
                        View Profile
                    </a>
                </div>
            @endguest
            @include('layouts.footer')
        </section>
    </div>
@endsection
