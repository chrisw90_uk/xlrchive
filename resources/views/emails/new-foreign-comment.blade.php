<html>
    <body>
        <p>Hello {{$original_post_author}}!</p>
        <p>{{ $comment_author }} has commented on your post on {{ $comment_author === $recipient ? 'their' : $recipient."'s'" }} profile.</p>
        <p><a href="{{ $app_url }}/login">Click here</a> to sign in and view this comment.</p>
    </body>
</html>
