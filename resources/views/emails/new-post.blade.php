<html>
    <body>
        <p>Hello {{$recipient_username}}!</p>
        <p>{{ $username }} has posted on your profile. Aren't you popular?</p>
        <p><a href="{{ $app_url }}/login">Click here</a> to sign in and view this post</p>
    </body>
</html>
