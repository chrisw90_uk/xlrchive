<?php

?>
<footer class="footer">
    <ul class="footer__links">
        <li>
            <a href="/terms">Terms and conditions</a>
        </li>
        <li>
            <a href="/privacy">Privacy policy</a>
        </li>
    </ul>
    <p class="footer__copyright margin--b-none">
        © <?php echo date('Y') === '2020' ? '2020' : '2020 - '.date('Y') ?> XLRchive.
    </p>
</footer>
