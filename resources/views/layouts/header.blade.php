<header class="header header--shadow bg--red">
    <div class="flex align--center justify--between wrapper header__inner">
        <div class="flex align--center">
            <a href="/" class="header__logo">XLRchive.</a>
            <nav class="header__site-nav">
                <a href="/search">
                    <i class="material-icons">
                        person_search
                    </i>
                    <span class="header__site-nav-label">
                        Search
                    </span>
                </a>
                @auth
                    <a href="/dashboard/notifications">
                        <i class="material-icons">
                            notifications
                        </i>
                        <span class="header__site-nav-label">
                            Notifications
                        </span>
                        <span id="notificationCount" class="header__alert"></span>
                    </a>
                    <script>
                        $(document).ready(function () {
                            var badge = $('#notificationCount');
                            function getNotificationCount() {
                                $.get( "/api/notifications/count", function(count) {
                                    if (count > 0) {
                                        badge.css('display', 'flex');
                                        if (count > 9) {
                                            badge.text('9+');
                                        } else {
                                            badge.text(count);
                                        }
                                    } else {
                                        badge.css('display', 'none');
                                    }
                                    setTimeout(function() {
                                        getNotificationCount();
                                    }, 30000)
                                });
                            }
                            getNotificationCount();
                        })
                    </script>
                @endauth
            </nav>
        </div>
        <div>
            @guest
                <a
                    href="/"
                    class="btn btn--outline btn--white-red"
                >
                    Login
                </a>
            @else
                <a
                    href="/dashboard"
                    class="btn btn--outline btn--hover-white"
                >
                    Dashboard
                </a>
                <a
                    href="/logout"
                    class="btn btn--outline btn--hover-white"
                >
                    Logout
                </a>
            @endguest
        </div>
    </div>
</header>
