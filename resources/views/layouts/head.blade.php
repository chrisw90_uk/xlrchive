<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex,nofollow">

    <title>{{ config('app.name', 'Xlrchive.') }}</title>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/site.js') }}" defer></script>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;300;800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
</head>
<body>
    <main>
        @yield('content')
    </main>
    <div id="divCookies" class="cookies">
        <h5 class="cookies__title">Cookies</h5>
        <div class="cookies__content">
            <p>
                XLRchive uses cookies to identify you upon login, keep track of your preferences and deliver ads relevant to your interests.
            </p>
            <p>
                By closing this message, you consent to our use of cookies as outlined in our
                <a target="_blank" href="/privacy">Privacy Policy</a>
            </p>
        </div>
        <button id="btnCookies" class="cookies__button btn btn--small btn--outline btn--hover-white">
            Close
        </button>
    </div>
</body>
</html>
