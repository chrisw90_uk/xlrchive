@extends('layouts.head')

@section('content')
    @include('layouts.header')
    <div class="full--w-footer flex align--center justify--center bg--off-black pad--x pad--y">
        <form class="form form--small" method="POST" action="{{ route('register') }}">
            @csrf
            <h2>Register.</h2>
            <div class="form__field">
                <input id="name" type="text" name="name" required autocomplete="name" placeholder="Name.">
                @error('name')
                    <span class="form__field-error" role="alert">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            <div class="form__field">
                <input id="username" type="text" name="username" required autocomplete="username" placeholder="Username.">
                <label for="username">
                    Your unique username which doubles as your profile URL.
                    <br />Only letters, numbers, dashes and underscores please.
                </label>
                @error('username')
                    <span class="form__field-error" role="alert">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            <div class="form__field">
                <input id="email" type="email" name="email" required autocomplete="email" placeholder="Email address.">
                @error('email')
                    <span class="form__field-error" role="alert">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            <div class="form__field">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password.">
                @error('password')
                    <span class="form__field-error" role="alert">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            <div class="form__field">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm password.">
            </div>
            <div class="text--right">
                <button type="submit" class="btn btn--outline btn--hover-red">
                    Register
                </button>
            </div>
        </form>
    </div>
    @include('layouts.footer')
@endsection
