@extends('layouts.head')

@section('content')
    @include('layouts.header')
    <div class="full--w-footer flex align--center justify--center bg--off-black pad--x pad--y">
        <form class="form form--small" method="POST" action="{{ route('password.update') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <h3>Password reset.</h3>
            <p>See, told you we've got you covered! Confirm your email address and new password below.</p>
            <div class="form__field">
                <input id="email" type="email" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="Email address.">
                @error('email')
                    <span class="form__field-error" role="alert">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            <div class="form__field">
                <input id="password" type="password" name="password" required autocomplete="new-password" placeholder="New password.">
                @error('password')
                    <span class="form__field-error" role="alert">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            <div class="form__field">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm password.">
            </div>
            <div class="text--right">
                <button type="submit" class="btn btn--outline btn--hover-red">
                    Reset Password
                </button>
            </div>
        </form>
    </div>
    @include('layouts.footer')
@endsection
