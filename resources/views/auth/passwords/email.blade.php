@extends('layouts.head')

@section('content')
    @include('layouts.header')
    <div class="full--w-footer flex align--center justify--center bg--off-black pad--x pad--y">
        <div>
            @if (session('status'))
                <h6 class="text--center text--green margin--b-xl" role="alert">
                    <strong>{{ session('status') }}</strong>
                </h6>
            @endif
            <form class="form form--small" method="POST" action="{{ route('password.email') }}">
                @csrf
                <h3>I've forgotten my password.</h3>
                <p>Don't worry about it, we've got you covered. Enter your account email address below and we'll send you a password reset link.</p>
                <div class="form__field">
                    <input id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email address.">
                    @error('email')
                        <span class="form__field-error" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>
                <div class="text--right">
                    <button type="submit" class="btn btn--outline btn--hover-red">
                        Send
                    </button>
                </div>
            </form>
        </div>
    </div>
    @include('layouts.footer')
@endsection
