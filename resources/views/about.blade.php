@extends('layouts.head')

@section('content')
    @include('layouts.header')
    <div class="page bg--off-black">
        <section class="page__header">
            <h1>About.</h1>
        </section>
        <section class="page__container pad--x">
            <article class="page__content">
                <div class="wrapper wrapper--small">
                    <p>Hello, and welcome to XLRchive!</p>

                    <h4>So what is it?</h4>

                    <p>
                        XLRchive is a website for performing artists, technicians and engineers to interact with other
                        members of the community and showcase their equipment and setups to fellow enthusiasts. Whether
                        you’re an aspiring amateur or a prolific professional, everyone is welcome in the community.
                    </p>

                    <h4>Who made it?</h4>

                    <p>
                        The site is a passion project of a single developer who moonlights as a professional composer for video games.
                        A developer by day and a composer by night, they're always seeking new ways of combining their two passions.
                    </p>

                    <h4>How does it work?</h4>

                    <p>
                        Users create a profile which they can customise with basic information such as a user category, social media handles, images and a bio.
                        Then, users can create lists of their equipment or gear. You can check out other users’ profiles by using the search feature,
                        with optional filters for name and category.
                    </p>

                    <h4>What's in store for the future?</h4>

                    <p>
                        Several new developments are in the pipeline to establish and grow positive interaction within the community.
                        These include an optional Reddit-style Q&A section, a portfolio and a virtual setup/tour.
                    </p>
                </div>
            </article>
            <aside class="page__sidebar">
                <h3 class="text--center">What's new?</h3>
                <ul class="page__sidebar-list">
                    <li class="page__sidebar-list-item">
                        <h6>12th January 2021 (v1.3.2)</h6>
                        <ul>
                            <li>Fixed a bug where the unread notification count didn't decrease when an unread notification was deleted.</li>
                            <li>Fixed a bug in the foreign comment email where the recipient name was repeated.</li>
                        </ul>
                    </li>
                    <li class="page__sidebar-list-item">
                        <h6>11th January 2021 (v1.3.1)</h6>
                        <ul>
                            <li>Notifications now display a date and a preview of the relevant post.</li>
                            <li>Users are now required to manually mark notifications as seen.</li>
                            <li>Posts on a user's profile display the number of replies.</li>
                        </ul>
                    </li>
                    <li class="page__sidebar-list-item">
                        <h6>18th December 2020 (v1.3.0)</h6>
                        <p>
                            A Discussion section has been added to each user profile. Users can now leave posts and comments which can also be upvoted.
                            Recipients of posts and comments will also receive basic notifications which are deleted after 28 days.
                        </p>
                    </li>
                    <li class="page__sidebar-list-item">
                        <h6>24th October 2020 (v1.2.3)</h6>
                        <ul>
                            <li>The links under the 'Login' button no longer merge with the footer on mobile resolutions</li>
                            <li>Authenticated users see a rudimentary dashboard rather than the login form.</li>
                            <li>Reactions have suitable size and spacing on mobile resolutions.</li>
                        </ul>
                    </li>
                    <li class="page__sidebar-list-item">
                        <h6>23rd October 2020 (v1.2.2)</h6>
                        <ul>
                            <li>'Add' buttons across all Equipment Categories no longer show loading states when a new piece of equipment is added.</li>
                            <li>Reactions and equipment images are now also deleted when a piece of equipment is deleted.</li>
                            <li>The icon on the Equipment dashboard button no longer vanishes on smaller resolutions.</li>
                        </ul>
                    </li>
                    <!--
                    <li class="page__sidebar-list-item">
                        <h6>22nd October 2020 (v1.2.1)</h6>
                        <p>Fixed 'Forgotten Password' process.</p>
                    </li>
                    <li class="page__sidebar-list-item">
                        <h6>20th October 2020 (v1.2.0)</h6>
                        <p>Other users can now post reactions to your gear. Options include like, love, awe and rock on!</p>
                    </li>
                    <li class="page__sidebar-list-item">
                        <h6>12th October 2020 (v1.1.0)</h6>
                        <p>This About section has been added, documenting what XLRchive is and its most recent developments.</p>
                    </li>
                    -->
                </ul>
            </aside>
        </section>
    </div>
    @include('layouts.footer')
@endsection


