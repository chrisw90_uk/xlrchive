@extends('layouts.head')

@section('content')
    <div id="app"></div>
    <script type="application/javascript" src="/js/app.bundle.js"></script>
    @include('layouts.footer')
@endsection
