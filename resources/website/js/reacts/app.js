import React from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';

const REACTS = {
    like: {
        name: 'Like',
    },
    love: {
        name: 'Love',
    },
    awe: {
        name: 'Awe',
    },
    metal: {
        name: 'Metal',
    },
};

const NO_REACTS = {
    like: 0,
    love: 0,
    awe: 0,
    metal: 0,
};

class Reacts extends React.Component {
    state = {
        isReacting: false,
        reactionCounts: {},
        currentReactionId: null,
        currentReactionType: null,
    };

    componentDidMount() {
        const {
            reactions,
            currentReactionId,
            currentReactionType,
        } = this.props;
        const reactionCounts = JSON.parse(reactions);
        this.setState({
            reactionCounts: Object.keys(reactionCounts).length > 0 ? reactionCounts : NO_REACTS,
            currentReactionId,
            currentReactionType
        });
    }

    chooseReaction = (type) => {
        const {
            equipmentId,
            reactingUserId,
        } = this.props;
        const {
            currentReactionId,
            currentReactionType,
        } = this.state;
        const data = {
            uuid: currentReactionId,
            reactionType: type,
            equipmentId,
            reactingUserId: reactingUserId
        };
        this.setState(prevState => ({
            isReacting: true,
            currentReactionType: type,
            reactionCounts: {
                ...prevState.reactionCounts,
                [currentReactionType]: currentReactionId
                    ? prevState.reactionCounts[currentReactionType] - 1
                    : prevState.reactionCounts[currentReactionType],
                [type]: prevState.reactionCounts[type] + 1,
            }
        }));
        axios.post('/api/reaction', data).then(res => {
            const { uuid } = res.data;
            this.setState(prevState => ({
                isReacting: false,
                currentReactionId: uuid || prevState.currentReactionId,
            }))
        }).catch(() => {
            this.setState(prevState => ({
                isReacting: false,
                reactionCounts: {
                    ...prevState.reactionCounts,
                    [currentReactionType]: currentReactionId
                        ? prevState.reactionCounts[currentReactionType] + 1
                        : prevState.reactionCounts[currentReactionType],
                    [type]: prevState.reactionCounts[type] - 1,
                }
            }));
        });
    };

    removeReaction = (type) => {
        const {
            currentReactionId,
            currentReactionType,
        } = this.state;
        this.setState(prevState => ({
            isReacting: true,
            currentReactionType: null,
            reactionCounts: {
                ...prevState.reactionCounts,
                [type]: prevState.reactionCounts[type] - 1,
            }
        }));
        axios.delete(`/api/reaction?id=${currentReactionId}`).then(() => {
            this.setState({
                isReacting: false,
                currentReactionId: null,
            })
        }).catch(e => {
            this.setState(prevState => ({
                isReacting: false,
                currentReactionType,
                reactionCounts: {
                    ...prevState.reactionCounts,
                    [type]: prevState.reactionCounts[type] + 1
                }
            }));
        });
    };

    render() {
        const {
            disableReactions,
            reactingUserId,
        } = this.props;
        const {
            reactionCounts,
            currentReactionType,
            isReacting,
        } = this.state;
        const disabled = disableReactions === 'true' || isReacting || !reactingUserId;
        return (
            <ul className="flex">
                {Object.keys(REACTS).map(r => (
                    <li
                        key={r}
                        className={`reacts__item reacts__item-${r} ${r === currentReactionType ? 'reacts__item--active' : ''}`}
                    >
                        <button
                            disabled={disabled}
                            className="btn btn--reset reacts__btn"
                            type="button"
                            onClick={() => {
                                if (r === currentReactionType) {
                                    this.removeReaction(r)
                                } else {
                                    this.chooseReaction(r)
                                }
                            }}
                        >
                            <img src={`/reacts/${r}.svg`} alt={REACTS[r].name} />
                            <span className="reacts__item-count">
                                {reactionCounts[r]}
                            </span>
                        </button>
                    </li>
                ))}
            </ul>
        )
    }
}

const reactions = document.getElementsByClassName('reacts');

for(let i = 0; i < reactions.length; i++){
    ReactDOM.render(
        <Reacts {...reactions[i].dataset} />,
        reactions[i]
    );
}
