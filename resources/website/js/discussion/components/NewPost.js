import React, { useState } from 'react';
import Button from '../../../../app/js/components/Button';

const NewPost = ({
    title,
    onSubmit,
    isSending,
}) => {
    const [value, setValue] = useState('');
    return (
        <li className="qa__item qa__item-new">
            <form
                className="qa__item-inner"
                onSubmit={async e => {
                    const success = await onSubmit(e, { value });
                    if (success) setValue('');
                }}
            >
                <h6>{title}</h6>
                <div className="flex flex--wrap align--end">
                    <textarea
                        value={value}
                        onChange={e => setValue(e.target.value)}
                        disabled={isSending}
                    />
                    <Button
                        type="submit"
                        working={isSending}
                        className="btn--outline btn--hover-blue"
                        disabled={!value || isSending}
                        label="Send"
                    >
                    </Button>
                </div>
            </form>
        </li>
    )
};

export default NewPost;
