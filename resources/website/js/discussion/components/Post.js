import React from 'react';
import moment from 'moment';
import NewPost from "./NewPost";
import axios from "axios";
import { ToastsStore } from "react-toasts";
import { Collapse } from 'react-collapse';

class Post extends React.Component {

    state = {
        isSubmittingComment: false,
        showDeleteConfirmation: false,
        comments: [],
        showComments: false,
    };

    componentDidMount() {
        const {
            comments,
        } = this.props;
        this.setState({
            comments,
        })
    }

    submitComment = (e, comment) => {
        e.preventDefault();
        const {
            pageAuthorId: authorId,
            pageRecipientId: recipientId,
            postAuthorId,
            uuid: postId,
        } = this.props;
        this.setState({ isSubmittingComment: true });
        const data = {
            authorId,
            recipientId,
            postAuthorId: postAuthorId.toString(),
            postId,
            ...comment,
        };
        return axios.post('/api/comment', data).then(res => {
            this.setState(prevState => ({
                isSubmittingComment: false,
                comments: [...prevState.comments, res.data]
            }));
            ToastsStore.success('Comment submitted successfully', 5000);
            return true;
        }).catch(err => {
            const { errors } = err.response.data;
            this.setState({ isSubmittingComment: false });
            ToastsStore.error(errors.join('\n'), 5000);
            return false;
        });
    };

    deleteComment = (uuid) => {
        const {
            comments,
        } = this.state;
        const originalComments = [...comments];
        this.setState(prevState => ({
            comments: prevState.comments.filter(c => c.uuid !== uuid),
        }));
        return axios.post('/api/comment/delete', { uuid }).then(() => {
            return true;
        }).catch(err => {
            const { errors } = err.response.data;
            this.setState({ comments: originalComments, hasVoted: true, });
            ToastsStore.error(errors.join('\n'), 5000);
            return false;
        });
    };

    submitVoteForComment = (commentId) => {
        const data = { commentId };
        const { comments: originalComments } = this.state;
        this.setState({
            comments: originalComments.map(c => (
                c.uuid === commentId
                    ? {
                        ...c,
                        has_voted_count: 1,
                        votes_count: c.votes_count + 1
                    }
                    : c
            ))
        });
        return axios.post('/api/vote', data).then(() => {
            return true;
        }).catch(err => {
            const { errors } = err.response.data;
            this.setState({
                comments: originalComments
            });
            ToastsStore.error(errors.join('\n'), 5000);
            return false;
        });
    };

    removeVoteFromComment = (commentId) => {
        const data = { commentId };
        const { comments: originalComments } = this.state;
        this.setState({
            comments: originalComments.map(c => (
                c.uuid === commentId
                    ? {
                        ...c,
                        has_voted_count: 0,
                        votes_count: c.votes_count - 1
                    }
                    : c
            ))
        });
        return axios.post('/api/vote/delete', data).then(() => {
            return true;
        }).catch(err => {
            const { errors } = err.response.data;
            this.setState({
                comments: originalComments
            });
            ToastsStore.error(errors.join('\n'), 5000);
            return false;
        });
    };

    render() {
        const {
            uuid,
            profile,
            author,
            authorImage,
            datetime,
            content,
            controls,
            isProfileUser,
            pageAuthorId,
            pageRecipientId,
            disableReply,
            onDelete,
            onVote,
            disableVote,
            onRemoveVote,
            hasVoted,
            votes,
        } = this.props;
        const {
            showComments,
            comments,
            showDeleteConfirmation,
            isSubmittingComment,
        } = this.state;
        return (
            <React.Fragment>
                <li className="qa__item">
                    <div className="qa__item-inner">
                        <div className="flex align--center">
                            <div className={`qa__item-vote ${hasVoted ? 'active' : ''}`}>
                                <button
                                    type="button"
                                    className="btn--reset"
                                    disabled={disableVote}
                                    onClick={() => !hasVoted ? onVote(uuid) : onRemoveVote(uuid) }
                                >
                                    <svg height='20' width='30'>
                                        <polygon points="15,0 30,20 0,20"/>
                                    </svg>
                                    <span>{votes}</span>
                                </button>
                            </div>
                            <div className="pad--r-sm">
                                <div className="qa__item-author">
                                    {isProfileUser && <img src="/img/star.svg" alt="Star to indicate the profile's user" />}
                                    <small>
                                        <a href={`/user/${author}`}>{author}</a> | {moment(datetime).fromNow()}
                                    </small>
                                </div>
                                <div className="qa__item-q">
                                    <a
                                        href={`/user/${author}`}
                                        className="qa__item-img"
                                    >
                                        {authorImage ? (
                                            <img
                                                src={authorImage}
                                                alt={author}
                                            />
                                        ) : (
                                            <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-user" width="70%" height="70%"
                                                 viewBox="0 0 24 24" strokeWidth="2" stroke="white" fill="none" strokeLinecap="round"
                                                 strokeLinejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z"/>
                                                <circle cx="12" cy="7" r="4"/>
                                                <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"/>
                                            </svg>
                                        )}
                                    </a>
                                    <p>
                                        {content}
                                    </p>
                                </div>
                            </div>
                            {comments && (
                                <button
                                    className="btn--reset qa__item-toggle"
                                    onClick={() => this.setState(prevState => ({
                                        showComments: !prevState.showComments
                                    }))}
                                >
                                    <i className="material-icons">
                                        {showComments ? 'expand_less' : 'expand_more'}
                                    </i>
                                </button>
                            )}
                        </div>
                        <div className="flex justify--between">
                            {controls && (
                                <small className="qa__item-controls">
                                    {showDeleteConfirmation ? (
                                        <span>
                                            Are you sure?
                                            {' '}
                                            <a onClick={async () => {
                                                const success = await onDelete(uuid);
                                                if (!success) this.setState({ showDeleteConfirmation: false });
                                            }}>
                                                Yes
                                            </a>
                                            {' '}
                                            |
                                            {' '}
                                            <a onClick={() => this.setState({ showDeleteConfirmation: false })}>
                                                No
                                            </a>
                                        </span>
                                    ) : (
                                        <a onClick={() => this.setState({ showDeleteConfirmation: true })}>Delete</a>
                                    )}
                                </small>
                            )}
                            {comments && comments.length > 0 && (
                                <small className="margin--left-auto">
                                    {`${comments.length} repl${comments.length !== 1 ? 'ies' : 'y'}`}
                                </small>
                            )}
                        </div>
                    </div>
                    <Collapse isOpened={showComments}>
                        <ul>
                            {comments && comments.map(c => (
                                <Post
                                    key={c.uuid}
                                    uuid={c.uuid}
                                    author={c.author}
                                    authorImage={c.author_image_url}
                                    datetime={c.created_at}
                                    content={c.content}
                                    controls={c.author_id === parseInt(pageAuthorId)}
                                    isProfileUser={c.author_id === parseInt(pageRecipientId)}
                                    disableReply
                                    onDelete={this.deleteComment}
                                    onVote={this.submitVoteForComment}
                                    onRemoveVote={this.removeVoteFromComment}
                                    votes={c.votes_count}
                                    hasVoted={c.has_voted_count}
                                />
                            ))}
                            {!disableReply && (
                                <React.Fragment>
                                    {!pageAuthorId ? (
                                        <li className="text--center margin--b-md">
                                            You must <a href={`/?returnUrl=%2Fuser%2F${profile}`}>login</a> to reply to this post.
                                        </li>
                                    ) : (
                                        <NewPost
                                            title="Reply."
                                            isSending={isSubmittingComment}
                                            onSubmit={this.submitComment}
                                        />
                                    )}
                                </React.Fragment>
                            )}
                        </ul>
                    </Collapse>
                </li>
            </React.Fragment>
        )
    }
}

export default Post;
