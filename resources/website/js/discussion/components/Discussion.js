import React from 'react';
import axios from 'axios';
import { ToastsContainer, ToastsContainerPosition, ToastsStore } from "react-toasts";
import NewPost from "./NewPost";
import Post from "./Post";

class Discussion extends React.Component {

    state = {
        isFetchingPosts: false,
        isSubmittingPost: false,
        posts: []
    };

    componentDidMount() {
        const {
            recipientId,
        } = this.props;
        this.setState({ isFetchingPosts: true });
        axios.get(`/api/posts/${recipientId}`).then(res => {
            console.log(res.data);
            this.setState({ isFetchingPosts: false, posts: res.data });
        }).catch(err => {
            const { errors } = err.response.data;
            this.setState({ isFetchingPosts: false });
            ToastsStore.error(errors.join('\n'), 5000);
        });
    }

    submitPost = (e, post) => {
        e.preventDefault();
        const {
            authorId,
            recipientId,
        } = this.props;
        this.setState({ isSubmittingPost: true });
        const data = { authorId, recipientId, ...post };
        return axios.post('/api/post', data).then(res => {
            this.setState(prevState => ({
                isSubmittingPost: false,
                posts: [res.data, ...prevState.posts]
            }));
            ToastsStore.success('Post submitted successfully', 5000);
            return true;
        }).catch(err => {
            const { errors } = err.response.data;
            this.setState({ isSubmittingPost: false });
            ToastsStore.error(errors.join('\n'), 5000);
            return false;
        });
    };

    submitVoteForPost = (postId) => {
        const data = { postId };
        const { posts: originalPosts } = this.state;
        this.setState({
            posts: originalPosts.map(p => (
                p.uuid === postId
                    ? {
                        ...p,
                        has_voted_count: 1,
                        votes_count: p.votes_count + 1
                    }
                    : p
            ))
        });
        return axios.post('/api/vote', data).then(() => {
            return true;
        }).catch(err => {
            const { errors } = err.response.data;
            this.setState({
                posts: originalPosts
            });
            ToastsStore.error(errors.join('\n'), 5000);
            return false;
        });
    };

    removeVoteFromPost = (postId) => {
        const data = { postId };
        const { posts: originalPosts } = this.state;
        this.setState({
            posts: originalPosts.map(p => (
                p.uuid === postId
                    ? {
                        ...p,
                        has_voted_count: 0,
                        votes_count: p.votes_count - 1
                    }
                    : p
            ))
        });
        return axios.post('/api/vote/delete', data).then(() => {
            return true;
        }).catch(err => {
            const { errors } = err.response.data;
            this.setState({
                posts: originalPosts
            });
            ToastsStore.error(errors.join('\n'), 5000);
            return false;
        });
    };

    deletePost = (uuid) => {
        const {
            posts,
        } = this.state;
        const originalPosts = [...posts];
        this.setState(prevState => ({
            posts: prevState.posts.filter(p => p.uuid !== uuid),
        }));
        return axios.post('/api/post/delete', { uuid }).then(() => {
            return true;
        }).catch(err => {
            const { errors } = err.response.data;
            this.setState({ posts: originalPosts });
            ToastsStore.error(errors.join('\n'), 5000);
            return false;
        });
    };

    render() {
        const {
            authorId,
            recipientId,
            profile,
        } = this.props;
        const {
            posts,
            isSubmittingPost
        } = this.state;
        return (
            <React.Fragment>
                {posts.length === 0 && <p className="text--center">Silence is golden.</p>}
                <ul>
                    {!authorId ? (
                        <li className="text--center margin--b-md">
                            You must <a href={`/?returnUrl=%2Fuser%2F${profile}`}>login</a> to leave a post.
                        </li>
                    ) : (
                        <NewPost
                            title="New post."
                            isSending={isSubmittingPost}
                            onSubmit={this.submitPost}
                        />
                    )}
                    {posts.map(p => (
                        <Post
                            key={p.uuid}
                            uuid={p.uuid}
                            profile={profile}
                            postAuthorId={p.author_id}
                            author={p.author}
                            authorImage={p.author_image_url}
                            datetime={p.created_at}
                            content={p.content}
                            controls={p.author_id === parseInt(authorId) || parseInt(authorId) === parseInt(recipientId)}
                            isProfileUser={p.author_id === parseInt(recipientId)}
                            comments={p.comments}
                            pageAuthorId={authorId}
                            pageRecipientId={recipientId}
                            onDelete={this.deletePost}
                            onVote={this.submitVoteForPost}
                            disableVote={!authorId}
                            onRemoveVote={this.removeVoteFromPost}
                            hasVoted={p.has_voted_count}
                            votes={p.votes_count}
                        />
                    ))}
                </ul>
                <ToastsContainer
                    position={ToastsContainerPosition.BOTTOM_RIGHT}
                    store={ToastsStore}
                />
            </React.Fragment>
        )
    }
}
export default Discussion;
