import React from "react";
import ReactDOM from "react-dom";
import Discussion from "./components/Discussion";

const el = document.getElementById('discussion');

ReactDOM.render(
    <Discussion {...el.dataset} />,
    el
);
