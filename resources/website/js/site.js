$(document).ready(function() {
    var cookiesAsArray = document.cookie.split('; ');
    var hideCookieNotice = false;
    cookiesAsArray.forEach(function(c) {
        var keyVal = c.split('=');
        if (keyVal[0] === 'cookie_notice_hidden') {
            hideCookieNotice = !!keyVal[1];
        }
    });

    if (hideCookieNotice) {
        $('#divCookies').remove();
    }

    // Hide cookie notice

    $('body').on('click', '#btnCookies', function() {
        $('#divCookies').remove();
        var now = new Date();
        now.setDate(now.getDate() + 365);
        document.cookie = "cookie_notice_hidden=true; expires=" + now.toUTCString();
    });
});

