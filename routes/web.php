<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'IndexController@index')->name('index');

Route::get('/search', 'SearchController@showUsers')->name('search');

Route::get('/terms', function () {
    return view('terms');
})->name('terms');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/privacy', function () {
    return view('privacy');
})->name('privacy');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/dashboard','HomeController@index');
    Route::get('/dashboard/{any}', 'HomeController@index')->where('any', '.*');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
});

// User

Route::get('/user/{username}', 'UserController@showUserProfile')->name('user');
Route::get('/api/user/details', 'UserController@getUserDetails');
Route::put('/api/user/details', 'UserController@saveUserDetails');
Route::get('/api/user/account', 'UserController@getAccountDetails');
Route::put('/api/user/account', 'UserController@saveAccountDetails');
Route::put('/api/user/password', 'UserController@saveNewPassword');
Route::put('/api/user/deactivate', 'UserController@deactivateAccount');
Route::put('/api/user/delete', 'UserController@deleteAccount');

// Equipment Categories

Route::get('/api/equipment-categories', 'EquipmentCategoryController@getEquipmentCategories');
Route::post('/api/equipment-categories', 'EquipmentCategoryController@saveEquipmentCategory');
Route::put('/api/equipment-categories', 'EquipmentCategoryController@updateEquipmentCategory');
Route::delete('/api/equipment-categories', 'EquipmentCategoryController@deleteEquipmentCategory');

// Equipment

Route::post('/api/equipment', 'EquipmentController@saveEquipment');
Route::put('/api/equipment', 'EquipmentController@updateEquipment');
Route::delete('/api/equipment', 'EquipmentController@deleteEquipment');

//Reactions

Route::put('/api/reactions', 'ReactionsController@toggleReactions');
Route::post('/api/reaction', 'ReactionsController@addOrUpdateReaction');
Route::delete('/api/reaction', 'ReactionsController@removeReaction');

//Posts

Route::get('/api/posts/{recipientId}', 'PostsController@getPosts');
Route::post('/api/post', 'PostsController@submitPost');
Route::post('/api/post/delete', 'PostsController@deletePost');

//Comments

Route::post('/api/comment', 'CommentsController@submitComment');
Route::post('/api/comment/delete', 'CommentsController@deleteComment');

//Votes

Route::post('/api/vote', 'VotesController@submitVote');
Route::post('/api/vote/delete', 'VotesController@deleteVote');

//Notifications

Route::get('/api/notifications', 'NotificationsController@getNotifications');
Route::get('/api/notifications/count', 'NotificationsController@getNotificationCount');
Route::post('/api/notification/delete', 'NotificationsController@deleteNotification');
Route::put('/api/notifications', 'NotificationsController@markNotificationsAsRead');
