const path = require('path');

module.exports = {
    entry: ['./resources/website/js/discussion/app.js'],
    watch: false,
    mode: 'production',
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    output: {
        filename: 'discussion.bundle.js',
        path: path.resolve(__dirname, 'public/js')
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: [
                    "url-loader"
                ]
            }
        ]
    }
};
