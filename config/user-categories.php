<?php

return [
    'producer' => 'Producer',
    'media_composer' => 'Media Composer',
    'classical_composer' => 'Classical Composer',
    'singer_songwriter' => 'Singer/Songwriter',
    'music_artist' => 'Music Artist',
    'dj' => 'DJ',
    'studio_audio' => 'Studio Engineer',
    'live_audio' => 'Live Audio Engineer',
    'live_lighting' => 'Lighting Engineer',
    'foley' => 'Foley/SFX Artist'
];
